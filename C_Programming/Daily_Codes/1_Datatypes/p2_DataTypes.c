// Program for Datatypes and size of datatypes in C

#include<stdio.h>

void main() {

	int age = 20;
	float petPrice = 105.85;
	double goldPrice = 47.3856465;
	char wing = 'A';

	printf("%d \n", age);
	printf("%.02f \n", petPrice);
	printf("%.07f \n", goldPrice);
	printf("%c \n", wing);
	
	//void v;
	

	printf("\nSize of Int datatype:: %ld \n", sizeof(int));
	printf("Size of Float datatype:: %ld \n", sizeof(float));
	printf("Size of double datatype is:: %ld \n", sizeof(double));
	printf("Size of Char datatype is:: %ld \n", sizeof(char));
	printf("Size of void datatype is:: %ld \n", sizeof(void));
}
