// Printing Address

#include<stdio.h>

void main() {

	int y = {10, 20, 30};

	printf("%d \n", y);
	printf("%p \n", &y);

	int z = (10, 20, 30);

	printf("%d \n", z);
	printf("%p \n", &z);	//%p is used to print address
}
