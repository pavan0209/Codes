// simple program showing global and local variable

#include<stdio.h>

int a = 10;	// global variable

void fun() {

	int x = 20;		//Local Variable
	printf("In Fun \n");
}

void main() {

	int y = 30;			//Local Variable
	printf("\nStart Main \n");
	fun();				//Call to function fun()
	printf("End Main\n\n");
}
