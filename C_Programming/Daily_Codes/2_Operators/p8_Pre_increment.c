//Operators - Pre-increment

#include<stdio.h>

void main()
{
	int x = 5;
	int output;

	printf("\n%d \n",x);
	printf("%d \n",output);

	output = ++x;		//Pre-increment

	printf("%d \n",x);
	printf("%d \n\n",output);
}
