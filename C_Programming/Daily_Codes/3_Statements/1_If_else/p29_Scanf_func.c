// Scanf() in C

#include<stdio.h>
void main(){

	int x;		//variable declaration

	printf("\nEnter the value of x::");
	scanf("%d",&x);				//taking value of x from user

	printf("Value of x is %d \n", x);	//print value stored in x

}
