// Program to check whether the character entered is UPPERCASE or LOWERCASE

#include<stdio.h>

void main() {

	char userData;
	
	printf("\nEnter character:: ");
	scanf("%c", &userData);
	
	printf("User Data = %c", userData);

	if(userData >= 'A' && userData <= 'Z') {

		printf("\nYou entered UPPERCASE character\n");
	} 

	if(userData >= 'a' && userData <= 'z') {

		printf("\nYou entered LOWERCASE character\n");
	}
}
