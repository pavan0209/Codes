//If statement

#include<stdio.h>

void main() {

	printf("Start Main \n");
	
	int x = 0;
	int y = 20;

	if(x) {
		printf("In first IF-block \n");
	}

	if(y) {
		printf("In second IF-block \n");
	}
	
	printf("End Main \n");
}
