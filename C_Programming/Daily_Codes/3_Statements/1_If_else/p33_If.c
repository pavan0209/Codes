
#include<stdio.h>

void main() {

	int x = 1;
	printf("\nStart Code");
	
	if(--x){				//0 = false and will go to next block
		printf("\nIn First block");
	}

	if(++x || x++){				//1
		printf("\nIn Second block");
	}

	printf("\nx= %d",x);			//1
	
	printf("\nEnd Main\n\n");

}
