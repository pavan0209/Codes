// Program to check whether the number is divisible by 2 using only if statement

#include<stdio.h>

void main() {

	int x;

	printf("\nEnter the value of x:: ");
	scanf("%d", &x);

	if(x % 2 == 0) {
		
		printf("\n %d is divisible by 2 \n\n", x);
	}
}
