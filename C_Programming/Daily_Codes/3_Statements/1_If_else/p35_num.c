//Program to check whether the number is divisible by 4 and 5 using IF statement only

#include<stdio.h>

void main() {

	int num;

	printf("\nEnter the number:: ");
	scanf("%d", &num);

	if(num % 4 == 0 && num % 5 == 0) {

		printf("%d is divisible by 4 and 5\n\n", num);
	}
}
