//Program to check whether the student is passed or fail on the basis of percentage using IF-ELSE statement

#include <stdio.h>

void main() {

	float percentage;
	
	printf("\nEnter your percentage:: ");
	scanf("%f", &percentage);

	if(percentage >= 35 && percentage <= 100) {

		printf("You are PASS with percentage %0.02f\n\n", percentage);
	}
	else {
		printf("You are FAIL \n\n");
	}
}
