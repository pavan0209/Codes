//Simple program using IF-ELSE statement

#include <stdio.h>

void main() {

	char ch;

	printf("\nAre you ALIVE??");
	printf("\nEnter y or n:: ");
	scanf("%c", &ch);

	if(ch == 'Y' || ch == 'y'){
		
		printf("Nice\n\n");
	}
	else{
		printf("User Died\n\n");
	}
}
