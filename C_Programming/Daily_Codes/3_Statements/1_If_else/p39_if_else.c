//Real world example of IF-ELSE statement

#include <stdio.h>

void main() {

	char ch;
	
	printf("\nDo you have laptop for CODING??");
	printf("\nEnter y or n:: ");
	scanf("%c", &ch);
	
	if(ch == 'Y' || ch == 'y') {

		printf("Enjoy CODING...\n\n");
	}
	else {
		
		printf("Find Another way to do CODING..\n\n");
	}
}


