//IF-ELSE Ladder

#include<stdio.h>

void main(){

	float pMoney;
	
	printf("\nEnter your Pocket Money:: ");
	scanf("%f", &pMoney);

	if(pMoney >= 2500){
		printf("Happipola\n\n");
	}
	else if(pMoney >= 2000){
		printf("CO2\n\n");
	}
	else if(pMoney >= 1000){
		printf("Sarovar\n\n");
	}
	else if(pMoney >= 500){
		printf("Vaishali\n\n");
	}
	else{
		printf("Mess ch jevan\n\n");
	}
}
