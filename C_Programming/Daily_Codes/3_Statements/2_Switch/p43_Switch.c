//Program Using SWITCH-CASE statement

#include <stdio.h>

void main() {

	int x;
	
	printf("\nEnter the value in range 1 - 5:: ");
	scanf("%d", &x);

	switch(x){
		
		case 1:
			printf("ONE \n\n");
			break;
		case 2:
			printf("TWO \n\n");
			break;
		case 3:
			printf("THREE \n\n");
			break;
		case 4:
			printf("FOUR \n\n");
			break;
		case 5:
			printf("FIVE \n\n");
			break;

		default:
			printf("Wrong Input \n\n");
	}
}





	
