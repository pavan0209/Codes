//Switch case only takes Integer Values
//Internally Character is converted into Integer in SWITCH CASE statement
//**This is because of constant value = means if we enter character while compile time compiler converts character into integer so it doesn't give any Error
//**If we enter character value while RUNTIME compiler doesn't convert into integer value compiler treat it as characterso it gives Error/ doesn't give expected output

#include<stdio.h>
void main(){

	int x;
	printf("\nEnter the value:: ");
	scanf("%d", &x);

	switch(x){
		
		case 65:
			printf("Value of x is 65\n\n");
			break;
		case 'A':				//ERROR: Duplicate case value
			printf("Value of x is A\n\n");
			break;
		case 66:
			printf("Value of x is 66\n\n");
			break;
		case 'B':				//ERROR: Duplicate case value
			printf("value of x is B\n\n");
			break;
			
		default:
			printf("Wrong Input\n\n");
	}
}
