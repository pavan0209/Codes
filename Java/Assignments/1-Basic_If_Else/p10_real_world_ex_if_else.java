/*
    Write a unique real-time example of if-else if-else ladder
*/

class Solution {
    public static void main(String[] args) {
        
        float budget = 110f;

        System.out.println();

        if(budget < 0) {

            System.out.println("Go back and get some money");
        }
        else if( budget <= 1.25f) {

            System.out.println("Go with average bike");
        }
        else if(budget < 2.2f) {

            System.out.println("Go with sport bike");
        }
        else {

            System.out.println("go with adventures/racing bike");
        }

        System.out.println();
}
}