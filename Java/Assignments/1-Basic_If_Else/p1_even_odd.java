/* 
    * Write a Java program to check if a number is even or odd

    ip: 10                      ip: 37
    op: 10 is EVEN no           op: 37 is ODD no
*/

class Solution {

    public static void main(String[] args) {
        
        int num = 15;

        System.out.println("\nNumber = " + num + "\n");

        if(num % 2 == 0) {

            System.out.println(num + " is EVEN number \n");
        }
        else {

            System.out.println(num + " is ODD number \n");
        }
    }
}