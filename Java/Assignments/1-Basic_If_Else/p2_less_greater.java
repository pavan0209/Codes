/* 
    *Write a java program, take a number, and print whether it is less than 10 or greater than 10

    ip: var = 5                 ip: var = 16
    op: 5 is less than 10       op: 16 is greater than 10
*/

class Solution {

    public static void main(String[] args) {
        
        int var = 10;

        System.out.println("\nnumber = " + var + "\n");

        if(var < 10) {

            System.out.println(var + " is less than 10");
        }
        else if(var > 10) {

            System.out.println(var + " is greater than 10");
        }
        else {

            System.out.println(var + " is equal to 10");
        }

        System.out.println();
    }
}