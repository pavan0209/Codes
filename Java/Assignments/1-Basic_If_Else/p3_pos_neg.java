/* 
    *Write a java program, take a number, and print whether it is Positive or Negative

    ip: 5                           ip: -6
    op: 5 is a positive number      op: -6 is a negative number
*/

class Solution {

    public static void main(String[] args) {
        
        int num = 10;

        System.out.println("\nNumber = " + num + "\n");

        if(num < 0) {

            System.out.println(num + " is Negative number");
        }
        else if(num > 0){

            System.out.println(num + " is Positive number");
        }
        else {

            System.out.println(num + " has no sign(neutral)");
        }

        System.out.println();
    }
}