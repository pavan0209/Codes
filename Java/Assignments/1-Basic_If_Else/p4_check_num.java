/* 
    *Write a Java program that checks a number from 0 to 5 and prints its spelling.
    If the number is greater than 5 print the number is greater than 5.

    ip: var = 4     ip: var = 6
    op: Four        op: number is greater than 5
*/

class Solution {

    public static void main(String[] args) {
        
        int var = 40;

        System.out.println("\nnumber = " + var + "\n");

        if(var < 0) {

            System.out.println("number is less than 0");
        }
        else if(var == 0) {

            System.out.println("Zero");
        }
        else if(var == 1) {

            System.out.println("One");
        }
        else if(var == 2) {

            System.out.println("Two");
        }
        else if(var == 3) {

            System.out.println("Three");
        }
        else if(var == 4) {

            System.out.println("Four");
        }
        else if(var == 5) {

            System.out.println("Five");
        }
        else {

            System.out.println("number is greater than 5");
        }

        System.out.println();
    }
}