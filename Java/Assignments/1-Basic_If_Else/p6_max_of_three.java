/* 
    *Write a program to find maximun between three numbers

    ip:                                
        num1 = 1                          
        num2 = 2
        num3 = 3
    op: 3 is the maximum between 1, 2 and 3.
*/

class Solution {

    public static void main(String[] args) {
        
        int num1 = 10; 
        int num2 = 50;
        int num3 = 10;

        if(num1 == num2 && num1 == num3) {

            System.out.println(num1 + " , " + num2 + " and " + num3 + " are equal" );
        }
        else if(num1 == num2 && num1 > num3) {

            System.out.println(num1 + " and " + num2 + " are equal and are maximum than " + num3 );
        }
        else if(num1 == num2 && num1 < num3) {

            System.out.println(num1 + " and " + num2 + " are equal and are less than " + num3 );
        }
        else if(num1 == num3 && num1 > num2) {

            System.out.println(num1 + " and " + num3 + " are equal and are maximum than " + num2 );
        }
        else if(num1 == num3 && num1 < num2) {

            System.out.println(num1 + " and " + num3 + " are equal and are less than " + num2 );
        }
        else if(num2 == num3 && num2 > num1) {
            
            System.out.println(num2 + " and " + num3 + " are equal and are maximum than " + num1 );
        }
        else if(num2 == num3 && num2 < num1) {
            
            System.out.println(num2 + " and " + num3 + " are equal and are less than " + num1 );
        }
        else if (num1 > num2 && num1 > num3) {

            System.out.println(num1 + " is maximun between " + num1 + " , " + num2 + " and " + num3);
        }
        else if (num2 > num1 && num2 > num3) {

            System.out.println(num2 + " is maximun between " + num1 + " , " + num2 + " and " + num3);
        }
        else {

            System.out.println(num3 + " is maximun between " + num1 + " , " + num2 + " and " + num3);
        }

    }


}