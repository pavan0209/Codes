/*
    Calculate Profit or loss

    Write a program that takes the cost price and selling price(take it harcoded) and calculate its profit or loss

    ip:                                 ip:
        selling_price = 1200                selling_price: 300
        cost_pricr = 1000                   cost_price: 500
    op:                                 op:
        profit of 200                       loss of 200
*/

class Solution {

    public static void main(String[] args) {
        
        int selling_price = 1200;
        int cost_price = 1500;

        System.out.println("\nSelling Price = " + selling_price);
        System.out.println("Cost Price = " + cost_price + "\n");

        if(selling_price == cost_price) {

            System.out.println("Neither profit nor loss");
        }
        else if(selling_price > cost_price) {

            System.out.println("profit of " + (selling_price-cost_price));
        }
        else {

            System.out.println("loss of " + (cost_price-selling_price));
        }

        System.out.println();
    }
}