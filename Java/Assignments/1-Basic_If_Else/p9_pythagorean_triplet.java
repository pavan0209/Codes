/*
    Write a program in java to accept three numbers and check whether they are pythagorean triplets or not

    ip :                                ip:
        a = 3                               a = 1
        b = 4                               b = 6
        c = 5                               c = 9
    op :                                op:
        It is a pythagorean triplet         It is not a pythagorean triplet
*/

class Solution {
    public static void main(String[] args) {
        
        int a = 1;
        int b = 6;
        int c = 9;

        System.out.println();

        if(a * a + b * b == c * c) {
               
            System.out.println("It is a pythagorean triplet");
        }
        else if (a * a + c * c == b * b ) {

            System.out.println("It is a pythagorean triplet");
        }
        else if (b * b + c * c == a * a ) {

            System.out.println("It is a pythagorean triplet");
        }
        else {

            System.out.println("It is not a pythagorean triplet");
        }

        System.out.println();
    }
}