/*
    Write a program to print the product of first 10 numbers
*/

class Solution {
    public static void main(String[] args) {
        
        int mult = 1;

        System.out.println();

        for(int i = 1; i <= 10; i++) {

            mult *= i;
        }

        System.out.println("mult of first 10 numbers :: " + mult);

        System.out.println();
    }
}