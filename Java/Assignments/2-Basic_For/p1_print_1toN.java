/*
    Write a program to print the first 10 numbers
*/

class Solution {
    public static void main(String[] args) {
        
        int N = 10;

        System.out.println("\nNumber = " + N);

        for(int i = 1; i <= 10; i++) {

            System.out.print(i + " ");
        }

        System.out.println();
    }
}