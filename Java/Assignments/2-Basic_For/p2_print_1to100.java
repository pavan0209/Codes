/*
    Write a program to print the first 100 numbers
*/

class Solution {
    public static void main(String[] args) {
        
        int N = 10;

        System.out.println("\nNumber = " + N);

        for(int i = 1; i <= 100; i++) {

            System.out.print(i + " ");
        }

        System.out.println();
    }
}