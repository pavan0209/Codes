/*
    Write a program to print the first Three digit numbers
*/

class Solution {
    public static void main(String[] args) {
        
        int N = 109;

        System.out.println("\nNumber = " + N);

        for(int i = 100; i <= N; i++) {

            System.out.print(i + " ");
        }

        System.out.println();
    }
}