/*
    Write a program to print the even numbers from 1 to 100
*/

class Solution {
    public static void main(String[] args) {
        
        int N = 100;

        System.out.println("\nNumber = " + N);

        /*1st way
        for(int i = 0; i <= N; i += 2) {

            System.out.print(i + " ");
        }*/

        /*2nd way */
        for(int i = 0; i <= N; i++) {

            if(i % 2 == 0) 
                System.out.print(i + " ");
        }   

        System.out.println();
    }
}