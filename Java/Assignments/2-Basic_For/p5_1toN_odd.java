/*
    Write a program to print the odd numbers 1 to 50
*/

class Solution {
    public static void main(String[] args) {
        
        int N = 50;

        System.out.println("\nNumber = " + N);

        /*1st way
        for(int i = 1; i <= N; i += 2) {

            System.out.print(i + " ");
        }*/

        /*2nd way */
        for(int i = 1; i <= N; i++) {

            if(i % 2 != 0) 
                System.out.print(i + " ");
        }   

        System.out.println();
    }
}