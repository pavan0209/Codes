/*
    Write a program to print reverse from 100 to 1
*/

class Solution {
    public static void main(String[] args) {
        
        int N = 100;

        System.out.println("\nNumber = " + N);

        for(int i = N; i >= 1; i--) {

            System.out.print(i + " ");
        }

        System.out.println();
    }
}