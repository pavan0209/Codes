/*
    Write a program to print table of 12 in reverse order
*/

class Solution {
    public static void main(String[] args) {

        int N = 12;

        System.out.println("\nNumber = " + N + "\n");

        for (int i = 10; i >= 1; i--) {

            System.out.println(N + " * " + i + " = " + (N * i));
        }

        System.out.println();
    }
}
