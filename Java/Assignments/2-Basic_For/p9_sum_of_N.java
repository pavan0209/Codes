/*
    Write a program to print the sum of first 10 numbers
*/

class Solution {
    public static void main(String[] args) {
        
        int sum = 0;

        System.out.println();

        for(int i = 1; i <= 10; i++) {

            sum += i;
        }

        System.out.println("Sum of first 10 numbers :: " + sum);

        System.out.println();
    }
}