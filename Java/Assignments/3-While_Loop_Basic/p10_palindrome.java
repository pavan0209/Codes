/* 
 
   Write a program to check whether the given number is palindrome number or not. 

   	input :: 2332

*/

class solution{

	public static void main(String[] args){
	
		int no = 2332;
		int rev = 0;
		int temp = no;

		while(no != 0){

			int rem = no % 10;
			rev = rev * 10 + rem;
			no = no / 10;
		}

		if(temp == rev){
			
			System.out.println(" palindrome number ");
		}
		else{

			System.out.println(" not a palindrome number ");
		}
	}
}
