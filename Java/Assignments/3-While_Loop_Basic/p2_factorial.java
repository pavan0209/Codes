/* 
 
   Write a program to calculate factorial of a given number.

   	input:: 6
	output :: Factorial of 6 is 720.

*/

class solution{

	public static void main(String[] args){

		int i = 1;
		int fact = 1;
		int no = 6;

		while(i <= no){

			fact = fact * i;
			i++;
		}
		System.out.println("Factorial of " + no + " is " + fact);
	}
}
