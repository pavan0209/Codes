/* 
 
   Write a program to count the digits of a given number.

   	input:: 942111423
	output :: count of digits = 9

*/

class solution{

	public static void main(String[] args){

		int no = 942111423;
		int count = 0;

		while(no != 0){
			
			count++;
			no = no / 10;
		}

		System.out.println("Count of digits is " + count);
	}
}
