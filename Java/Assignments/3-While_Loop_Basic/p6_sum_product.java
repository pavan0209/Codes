/* 
 
   Write a program to print the sum of all even numbers and multiplication of odd numbers between 1 to 10.

	output :: sum of even numbers between 1 to 10 = 30
		  multiplicationn of odd numbers between 1 to 10 = 945

*/

class solution{

	public static void main(String[] args){
		
		int i = 1;
		int no = 10;
		int sum = 0;
		int product = 1;

		while(i <= no){

			if(i % 2 == 0){

				sum = sum + i;
			}
	
			else{

				product = product * i;
			}
			
			i++;
		
		}
			System.out.println("sum = " + sum + "  "  +"Multiplication = " + product);

	}
}
