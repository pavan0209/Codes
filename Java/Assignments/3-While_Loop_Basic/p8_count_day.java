/* 
 
   Write a program to print the countdown of days to submit assignment.
	
   	input :: day = 7 
	output :: 7 days remaining
		  6 days remaining
		  .
		  .
		  .
		  .
		  0 days remaining.
	
*/

class solution{

	public static void main(String[] args){
		
		int day = 7;

		while(day >= 1){

			System.out.println( day + "  "  + "days  remaining. ");
			day--;
		}
		
		
		System.out.println( " 0 days Assignment overdue. ");

	}
}
