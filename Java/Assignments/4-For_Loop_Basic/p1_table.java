/* 
   Write a program to print a table of 2.
   	
   	output :: 2   4    6   8   10   12   14   16   18   20

*/

class solution{

	public static void main(String[] args){

		for(int i = 1; i <= 10; i++) {

			System.out.print((i * 2) + "  ");
			
		}

		System.out.println();
	}
}
