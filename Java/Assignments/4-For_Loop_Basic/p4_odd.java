/* 
 
   Write a program to count the odd digits of a given number.

   	input:: 942111423
	output :: count of odd digits = 5

*/

class solution{

	public static void main(String[] args){

		int no = 942111423;
		int count = 0;

		while(no != 0){

			int rem = no % 10;
			no = no / 10;

			if(rem % 2 != 0){

				count++;
			}
		}

		System.out.println("Count of odd digits is " + count);
	}
}
