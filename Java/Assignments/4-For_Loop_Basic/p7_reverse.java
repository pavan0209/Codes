/* 
 
   Write a program which takes number from user if number is even print that number in reverse order or if the number is odd then print that number in reverse order by difference 2.

   	input :: 6
	output :: 6   5   4   3   2   1

	input :: 7
	output :: 7   5    3    1

*/

class solution{

	public static void main(String[] args){
	
		int no = 7;

		if(no % 2 == 0){

			while(no != 0){

				System.out.print(no + "  " );
				no--;
			}
		}
		else{

			while(no >= 1){

				System.out.print(no + "  ");
				no = no - 2;
			}			
		}

		System.out.println();
	}
}
