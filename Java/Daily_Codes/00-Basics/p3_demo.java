/* 
	Correct Way to write main method:

 	public static void main(String[] args){ }; 
*/


class Demo {

	public static void main(String[] args) {

		System.out.println("\nDemo Class\n");
	}
}
