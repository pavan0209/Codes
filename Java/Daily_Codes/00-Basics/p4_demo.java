/* Correct Way to write main method:
 
 	public static void main(String[] args){ }; but 
	static public void main(String[] args){ }; is also accepted
*/


class Demo {

	static public void main(String[] args) {

		System.out.println("\nDemo Class\n");
	}
}
