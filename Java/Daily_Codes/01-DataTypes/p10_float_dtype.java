/* 
   Float Datatype 
   size of float = 4 bytes
*/

class Dtype {

	public static void main(String[] args) {
			
		float f1 = 7.5;		//Error: incompatible types: possible lossy conversion from double to float
		float f2 = 7.5;		//Error

		System.out.println("f1 = " + f1);
		System.out.println("f2 = " + f2);

	}
}
