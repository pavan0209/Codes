/* 
   Instance Variable(Global Variable)
*/

class InstanceVar {
	
	int x = 10;

	public static void main(String[] args) {

		int y = 20;

		System.out.println("x = " + x);	//error: non-static variable x cannot be referenced from a static context
		System.out.println("y = " + y);

	}
}
