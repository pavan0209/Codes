/* 
   Integer Datatype 
   size of int = 4bytes
*/

class Dtype {

	public static void main(String[] args) {
	
		//jerNo = 45	//Error: cannot find symbol
		
		int jerNo = 45;

		System.out.println("jerNo = " + jerNo);
		System.out.println("size of int = " + (Integer.SIZE/8) + " bytes");
	}
}
