/*
    Byte is a datatype in java
    size of byte datatype is 1 byte 
*/

class Dtype {

	public static void main(String[] args) {

		byte var1 = 18;
		byte var2 = 18;

		System.out.println("var1 = " +var1);
		System.out.println("var2 = " +var2);
		
		//var1 = var1 + var2;	//Error: Incompatible type: Possible lossy conversion from int to byte
					//the number which is generated at runtime by java is by default "int" so
					//if we put integer value in "byte" it is incompatible

		System.out.println("var1 = " +var1);
		System.out.println("var2 = " +(var2+var1));
	}
}

