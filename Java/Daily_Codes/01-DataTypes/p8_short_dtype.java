/*
    short is a datatype in java
    size of short datatype is 2 byte 
*/

class Dtype {

	public static void main(String[] args) {

		short var1 = 18;
		short var2 = 18;

		System.out.println("var1 = " +var1);
		System.out.println("var2 = " +var2);
		
		//var1 = var1 + var2;	//Error: Incompatible type: Possible lossy conversion from int to byte
					//the number which is generated at runtime by java is by default "int" so
					//if we put integer value in "byte" it is incompatible

		System.out.println("size of short = " +(var1/8) + " bytes");
		System.out.println("var1+var2 = " +(var2+var1) + "\n");
	}
}

