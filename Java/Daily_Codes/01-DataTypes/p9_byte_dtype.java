/*
    Byte is a datatype in java
    size of byte datatype is 1 byte 
*/

class Dtype {

	public static void main(String[] args) {

		byte var1 = 18;
		byte var2 = 18;

		System.out.println("var1 = " + var1);
		System.out.println("var2 = " + var2);
		
		int var3 = var1 + var2;

		System.out.println("var1 = " + var1);
		System.out.println("var2 = " + var2);
		System.out.println("var3 = " + var3);
		System.out.println("add of var1 and var2 = " + (var1+var2));
	}
}

