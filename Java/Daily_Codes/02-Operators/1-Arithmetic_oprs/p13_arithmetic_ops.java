/* 
   Arithmetic Operators in Java::
  	
       1. Addition
       2. Substraction
       3. Division
       4. Multiplication
       5. Modulus       
*/

class Arithmetic {

	public static void main(String[] args) {

		int x = 10;
		int y = 20;

		System.out.println("\nAddition: " + (x+y));
		System.out.println("Substraction: " + (x-y));
		System.out.println("Multiplication: " + (x*y));
		System.out.println("Division: " + (x/y));
		System.out.println("Modulus: " + (x%y) + "\n");
	}
}
