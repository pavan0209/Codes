/*
   Relational Operations:
       
	1. less than (<)
	2. greater than (>)
	3. less than equal to (<=)
	4. greater than equal to (>=)
	5. equal equal to (==)
	6. not equal to (!=)
*/

class Relational {

	public static void main(String[] args) {

		int x = 10;
		int y = 20;
		
		System.out.println("\nx = " + x);
		System.out.println("y = " + y + "\n");

		System.out.println("is less than:: " + (x < y));
		System.out.println("is greater than:: " + (x > y));
		System.out.println("is less than or equal to:: " + (x <= y));
		System.out.println("is greater than or equal to:: " + (x >= y));
		System.out.println("are equal:: " + (x == y));
		System.out.println("is not equal to:: " + (x != y) + "\n");

	}
}
