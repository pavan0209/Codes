/*  
  less than operator in java (<)

*/

class Relational {

	public static void main(String[] args) {

		int x = 10;
		int y = 20;
		
		System.out.println("\nx = " + x);
		System.out.println("y = " + y);

		if(x < y) 
			System.out.println("\nHello\n");
		else
			System.out.println("\nHiiii\n");
	}
}
