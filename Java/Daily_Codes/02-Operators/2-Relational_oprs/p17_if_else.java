/*  
    using relational operators in if else Statement
*/

class Relational {

	public static void main(String[] args) {

		int x = 10;
		int y = 20;
		
		System.out.println("\nx = " + x);
		System.out.println("y = " + y);

		if(x == y) 
			System.out.println("\nHello");
		else
			System.out.println("\nHiiii");
	
		
		if(true) 	 	//if statement works on boolean values only in java
			System.out.println("\nHello\n");

	}
}
