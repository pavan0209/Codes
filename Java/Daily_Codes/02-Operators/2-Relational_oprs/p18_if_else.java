/*  
    using relational operators in if else Statement
    if statement works only on boolean values
*/

class Relational {

	public static void main(String[] args) {

		int x = 10;
		
		System.out.println("\nx = " + x);

		if(x)		//error: incompatible types: int cannot be converted to boolean  
			System.out.println("\nHello");
		else
			System.out.println("\nHiiii");

	}
}
