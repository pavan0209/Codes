/* 
	Unary Operator in java
	types of increment: 
	  1: Pre-increment
	  2: Post-increment

	types of decrement:
	  1: Pre-decrement
	  2: Post-decrement
*/


class Unary {

	public static void main(String[] args) {

		int x = 5;
		int y = 7;

		System.out.println("\nx = " + x);
		System.out.println("y = " + y);

		System.out.println("\n++x = " + ++x);
		System.out.println("++y = " + ++y);

		System.out.println("\n--x = " + --x);
		System.out.println("--y = " + --y);

		System.out.println("\nx = " + x);
		System.out.println("y = " + y + "\n");
	}
}
