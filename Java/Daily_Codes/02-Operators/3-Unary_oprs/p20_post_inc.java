/* 
	Post increment
*/


class Unary {

	public static void main(String[] args) {

		int x = 5;
		int y = 7;

		System.out.println("\nx = " + x);
		System.out.println("y = " + y);

		System.out.println("\nx++ = " + x++);
		System.out.println("y++ = " + y++);

		System.out.println("\nx = " + x);
		System.out.println("y = " + y + "\n");
	}
}
