/* 
	Post increment and Post decrement
*/


class Unary {

	public static void main(String[] args) {

		int x = 15;
		int y = 71;

		System.out.println("\nx = " + x);
		System.out.println("y = " + y);

		System.out.println("\nx++ = " + x++);
		System.out.println("y++ = " + y++);

		System.out.println("\nx-- = " + x--);
		System.out.println("y-- = " + y--);
		
		System.out.println("\nx = " + x);
		System.out.println("y = " + y + "\n");
	}
}
