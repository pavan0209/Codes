/*
	Pre-increment and Post-increment
	Pre-decrement and Post-decrement
*/

class Unary {

	public static void main(String[] args) {

		int x = 8;
		int y = 5;

		System.out.println("\nx = " + x);
		System.out.println("y = " + y);

		int ans1 = ++x + x++;
		int ans2 = --y + y--;
		
		System.out.println("\nans1 = " + ans1);
		System.out.println("ans2 = " + ans2);

		System.out.println("\nx = " + x);
		System.out.println("y = " + y + "\n");
	}
}
