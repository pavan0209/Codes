/*
	Operations on Pre-increment and Post-increment
*/

class Unary {

	public static void main(String[] args) {

		int x = 10;
		int y = 20;

		System.out.println("\nx = " + x);
		System.out.println("y = " + y);

		int ans = ++x + y++ + x++;

		System.out.println("\nx = " + x);
		System.out.println("y = " + y);
		System.out.println("ans = " + ans + "\n");
	}
}
