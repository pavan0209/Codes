/*
	Logical Operators in java::
	   1. Logical AND (&&)
	   2. Logical OR (||)
*/

class Logical {

	public static void main(String[] args) {

		int x = 5;
		int y = 7;

		//int ans1 = x && y;	//error: bad operand types for binary operator '&&'

		//int ans2 = x<y && y>x;	//error: incompatible types: boolean cannot be converted to int
		
		boolean ans3 = x<y && y>x;
		
		System.out.println(ans3);
		System.out.println(x<y && y>x);
	}
}

