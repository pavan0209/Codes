/*
	Logical Operators in java::
	   1. Logical AND (&&)
	   2. Logical OR (||)
*/

class Logical {

	public static void main(String[] args) {

		int x = 5;
		int y = 7;

		boolean ans1 = x<y && y<x; 	//Logical AND
		boolean ans2 = x<y || y<x;	//Logical OR
		
		System.out.println("\nans1:: " + ans1);
		System.out.println("ans2:: " + ans2 + "\n");
	}
}

