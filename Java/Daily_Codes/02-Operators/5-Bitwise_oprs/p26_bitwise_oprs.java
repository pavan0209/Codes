/*
 	Bitwise Operators in Java
	  1. Bitwise AND (&)
	  2. Bitwise OR (|)
	  3. Bitwise XOR (^)
	  4. Bitwise Left Shift (<<)
	  5. Bitwise Right Shift (>>)
	  6. Bitwise Zero Fill Right Shift (>>>)
	  7. Bitwise Complement (~)
*/

class Bitwise {

	public static void main(String[] args) {
	
		int x = 5;
		int y = 7;

		System.out.println(x & y);	//Bitwise AND
		System.out.println(x | y);	//Bitwise OR
	}
}
