/*
 	Negation Operator(~) (Bitwise Complement Operator)
*/

class Bitwise {

	public static void main(String[] args) {
	
		int x = 2;

		System.out.println(~x);
	}
}
