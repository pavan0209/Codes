/*
     Bitwise Right Shift Operator(>>) and Zero fill right shift operator(>>>)
*/

class Bitwise {

    public static void main(String[] args) {
    
        int x = 7; 

        System.out.println(x >> 2);     //right shift
        System.out.println(x >>> 2);    //Zero fill right shift
    }
}