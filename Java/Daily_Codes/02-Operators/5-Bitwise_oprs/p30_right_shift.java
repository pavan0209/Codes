/*
     Bitwise Right Shift Operator(>>) and Zero fill right shift operator(>>>)
*/

class Bitwise {

    public static void main(String[] args) {
    
        int x = -7; 

        System.out.println(x >> 2);     //-2
        System.out.println(x >>> 2);    //1073741822 (this is because of negative value)
        System.out.println(x >>> 31);   //1
    }
}