/*
     Ternary Operator (? :)
*/

class Ternary {

    public static void main(String[] args) {
    
        int x = 10; 
        int y = 20;

        System.out.println((x < y) ? x : y);       //10
        /*  if(x < y)
                System.out.println(x);
            else
                System.out.println(y);
        */
    }
}