/*
    Control Statements in java:
        1. if statement
        2. if-else statement
        3. if-else-if ladder
        4. for loop
        5. while loop
        6. do while loop
        7. nested if-else
        8. nested for and while loop
        9. switch statement
    
    //If statement
    Problem Statement
    *Given an interger age as input
    *Print "Eligible to vote" if the person is eligible to vote
     
*/

class Solution {

    public static void main(String[] args) {
        
        int age = 19; 

        if(age > 18){

            System.out.println("\nEligible to vote\n");
        }
    }
}