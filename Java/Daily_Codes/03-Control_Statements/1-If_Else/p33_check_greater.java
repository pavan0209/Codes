/* 
    Write a Java Program to check that the number is greater than 5 or not using only if statement

*/

class Solution {

    public static void main(String[] args) {
        
        int x = 10;

        if(x > 5) {

            System.out.println("\nGreater than 5");
        }
        System.out.println("Outside if\n");
    }
}