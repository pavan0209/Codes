/* 
    *Take two integers A and B as input
    *print the max of two
    *Assume x and y are not equal

    ip: x = 5
        y = 7
    op: 7 is greater
*/

class Solution {

    public static void main(String[] args) {
        
        int x = 10; 
        int y = 7;

        System.out.println("\nx = " + x);
        System.out.println("y = " + y + "\n");

        if(x > y) {
            System.out.println(x + " is greater\n");
        }
        else {
            System.out.println(y + " is greater\n");
        }
    }
}