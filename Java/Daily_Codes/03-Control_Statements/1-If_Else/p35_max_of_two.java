/* 
    *Take two integers A and B as input
    *print the max of two

    ip: x = 5               ip: x = 5
        y = 7                   y = 5
    op: 7 is greater        op: both are equal
*/

class Solution {

    public static void main(String[] args) {
        
        int x = 10; 
        int y = 10;

        System.out.println("\nx = " + x);
        System.out.println("y = " + y + "\n");

        if(x == y) {
            System.out.println("Both are equal\n");
        }
        else if(x > y) {
            System.out.println(x + " is greater\n");
        }
        else {
            System.out.println(y + " is greater\n");
        }
    }
}