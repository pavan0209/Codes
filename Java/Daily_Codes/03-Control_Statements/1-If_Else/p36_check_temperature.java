/* 
    *Given the temperature of a person in farenheit.
    * Print whether the person has high, normal or low temperature

    >98.6 ---------------------> High
    >=98.0 and <= 98.06 ------->Normal
    <98.0 --------------------->Low
*/

class Solution {

    public static void main(String[] args) {
        
        float temp = 97.8f;

        System.out.println("\ntemperature = " + temp + "\n");

        if(temp >= 98 && temp <= 98.6f) {

            System.out.println("Normal\n");
        }
        else if(temp < 98.0f){

            System.out.println("Low\n");
        }
        else {

            System.out.println("High\n");
        }
    }
}