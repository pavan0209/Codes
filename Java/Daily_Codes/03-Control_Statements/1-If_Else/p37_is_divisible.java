/*
    *Take an integer as input and print whether the number is divisible by 4 or not

    ip: 5
    op: Not Divisible
*/

class Solution {

    public static void main(String[] args) {
        
        int num = 20;

        System.out.println("\nNumber = " + num + "\n");

        if(num % 4 == 0) {
            
            System.out.println("Divisible by 4\n");
        }
        else {

            System.out.println("Not Divisible by 4\n");
        }

    }
}