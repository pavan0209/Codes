/* 
    Odd or Even
    *Given an integer check that the number is odd or even

    ip: 20                      ip: 19
    op: 20 is EVEN no           op: 19 is ODD no
*/

class Solution {

    public static void main(String[] args) {
        
        int num = 15;

        System.out.println("\nNumber = " + num + "\n");

        if(num % 2 == 0) {

            System.out.println(num + " is EVEN number \n");
        }
        else {

            System.out.println(num + " is ODD number \n");
        }
    }
}