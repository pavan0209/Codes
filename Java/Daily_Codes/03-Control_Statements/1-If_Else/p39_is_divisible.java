/*
    *Take an integer as input 
    --Print 'Fizz' if it is divisible by 3
    --Print 'buzz' if it is divisible by 5
    --Print 'Fizz-buzz' if it is divisible by both
    --If not then print "Not Divisible by both"

    ip: 5       ip: 15          ip: 9       ip: 11
    op: buzz    op: Fizz-buzz   op: Fizz    op: Not Divisible by both
*/

class Solution {

    public static void main(String[] args) {
        
        int num = 31;

        System.out.println("\nNumber = " + num + "\n");

        if(num % 3 == 0 && num % 5 == 0) {
            
            System.out.println("Fizz-buzz\n");
        }
        else if(num % 3 == 0){

            System.out.println("Fizz\n");
        }
        else if(num % 5 == 0){

            System.out.println("buzz\n");
        }
        else {
            System.out.println("Not Divisible by both\n");
        }

    }
}