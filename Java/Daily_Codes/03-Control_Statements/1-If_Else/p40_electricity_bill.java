/*
    *Electricity Bill Problem
    --Calculate and print the bill amount

    units <= 100 ---> Price per unit is 1
    units > 100 ----> Price per unit is 2

    ip: 50      ip: 200 
    op: 50      op: 300 (100 for first 100 units and 200 for another 100 units)
*/

class Solution {

    public static void main(String[] args) {

        int units = 500;

        System.out.println("\nUnits = " + units + "\n");

        if(units <= 100) {

            System.out.println("bill = " + units + "\n");
        }
        else {

            System.out.println("bill = " + (units * 2 - 100) + "\n");
        }

    }
}