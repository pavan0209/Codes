/* 
    while loop in java
*/

class Solution {

    public static void main(String[] args) {
        
        int i = 1;

        System.out.println("\n");

        while(i <= 5) {

            System.out.println("Hello");        //infinite loop as their is not terminating condition
        }

        System.out.println("\n");
    }
}