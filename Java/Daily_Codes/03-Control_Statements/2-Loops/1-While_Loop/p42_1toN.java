/* 
    *print integers from 1 to 10 using while loop
*/

class Solution {

    public static void main(String[] args) {
        
        int i = 1;

        System.out.println();

        while(i <= 10) {

            System.out.println(i++);  
        }

        System.out.println();
    }
}