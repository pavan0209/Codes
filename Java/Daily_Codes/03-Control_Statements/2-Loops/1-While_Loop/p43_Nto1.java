/* 
    *print integers in reverse from 10 to 1 using while loop
*/

class Solution {

    public static void main(String[] args) {
        
        int i = 10;

        System.out.println();

        while(i >= 1) {

            System.out.println(i--);        
        }

        System.out.println();
    }
}