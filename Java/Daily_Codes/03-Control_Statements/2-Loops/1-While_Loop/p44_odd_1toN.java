/* 
    *Take an integer N as input
    --Print odd numbers from 1 to N using loop

    ip: 10
    op: 1 3 5 7 9
*/

class Solution {

    public static void main(String[] args) {
        
        int i = 1;

        System.out.print("\nOdd numbers:: ");

        while(i <= 10) {

            System.out.print(i + " ");
            i = i + 2;
        }
        
        /* Another way

            while( i <= 10){
                if(i % 2 != 0) {
                    System.out.print(i + " ");
                }
                i++;
            }
        */

        System.out.println("\n");
    }
}