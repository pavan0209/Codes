/* 
    *Take integer N as input 
    --Print multiples of 4 till N

    ip: 22
    op: 4 8 12 16 20
*/

class Solution {

    public static void main(String[] args) {
        
        int N = 22;
        int i = 4;

        System.out.print("\nMultiples of 4 :: ");

        while(i <= N) {

            System.out.print(i + " ");
            i = i + 4;
        }
        /* Another way
            i = 1;
            while(i <= N) {

                if(i % 4 == 0) {
                    System.out.print(i + " ");
                }
                i++;
            }
        */
        System.out.println("\n");
    }
}