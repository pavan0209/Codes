/* 
    print all the numbers from 1 to N which are divisible by 3

    ip : 10
    op : 3 6 9
*/

class Solution {
    public static void main(String[] args) {
        
        int i = 1;
        int N = 50;

        System.out.println();

        while(i <= N) {

            if(i % 3 == 0) {

                System.out.println(i + " ");
            }
            i++;
        }
        System.out.println();
    }
}