/*
    *Given an integer N
    *print all its digit

    ip : 6531
    op: 
        1
        3
        5
        6
*/

class Solution {
    public static void main(String[] args) {
        
        int N = 6531;
        System.out.println("\nNumber = " + N + "\n");
        while(N != 0) {

            int rem = N % 10;
            System.out.println(rem);
            N = N / 10;
        }

        System.out.println();
    }
}