/*
    *Given an Integer N
    *Print sum of digits

    Assume: n > 0

    ip : 6531
    op : 15
*/

class Solution {
    public static void main(String[] args) {
        
        int N = 6531;
        int sum = 0;

        System.out.println("\nNumber = " + N + "\n");

        while(N != 0) {

            int rem = N % 10;
            sum = sum + rem;
            N = N / 10;
        }

        System.out.println("Sum = " + sum + "\n");
    }
}