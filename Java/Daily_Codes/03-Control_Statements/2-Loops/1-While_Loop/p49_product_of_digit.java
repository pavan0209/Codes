/* 
    *print product of digits
    assume: n > 0;

    ip : 135
    op : 15
*/

class Solution {
    public static void main(String[] args) {

        int N = 135;
        int product = 1;

        System.out.println("\nNumber = " + N + "\n");

        while (N != 0) {

            int rem = N % 10;
            product = product * rem;
            N = N / 10;
        }

        System.out.println("Product of digits = " + product + "\n");
    }
}