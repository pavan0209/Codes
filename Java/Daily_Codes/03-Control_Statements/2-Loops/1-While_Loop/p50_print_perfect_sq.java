/* 
    *Take an integer N as input
    *Print perfect squares til N

    25 --> 5 : yes
    81 --> 9 : yes
    1  --> 1 : yes
    10 --> 3.13 : no

    ip : 30
    op : 1 4 9 16 25
*/

class Solution {
    public static void main(String[] args) {
        
        int N = 30;
        int i = 1;

        while (i*i <= N) {

            System.out.println((i*i) + " ");
            i++;
        }

        System.out.println();
    }
}