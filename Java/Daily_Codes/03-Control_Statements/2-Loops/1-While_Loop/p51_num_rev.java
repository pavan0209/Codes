/* 
    Given an Integer N as input
    *Print reverse of given number

    ip : 6531
    op : 1356
*/

class Solution {
    public static void main(String[] args) {
        
        int N = 6531;
        int rev = 0;

        System.out.println("\nNumber = " + N);

        while(N != 0) {

            int rem = N % 10;
            rev = rev * 10 + rem;
            N = N / 10;
        }
        System.out.println("Reverse Number = " + rev + "\n");
    }   
}