/*
    Take N as input. print from 1 to N
    ip : 5
    op : 1   2   3   4   5
*/
class Solution {
    public static void main(String[] args) {

        int N = 5;

        for(int i = 1; i <= N; i++) {

            System.out.print(i + " ");
        }

        System.out.println();
    }
}