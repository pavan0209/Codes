/*
    Take N as input. Print odd integers from 1 to N
    ip : 6
    op : 1  3  5 
*/

class Solution {
    public static void main(String[] args) {

        int N = 6;

        for(int i = 1; i <= N; i += 2) {

            System.out.print(i + "  ");
        }
        System.out.println();
    }
}