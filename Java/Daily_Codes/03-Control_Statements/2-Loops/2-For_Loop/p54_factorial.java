/*
    Take N as input. Print its factorial
    
    ip : 5          ip : 4 
    op : 120        op : 24
*/

class Solution {
    public static void main(String[] args) {

        int N = 5;
        int fact = 1;

        for(int i = 1; i <= N ; i++) {

            fact = fact * i;
        }

        System.out.println("Factorial of " + N + " is :: "+ fact);
    }
}