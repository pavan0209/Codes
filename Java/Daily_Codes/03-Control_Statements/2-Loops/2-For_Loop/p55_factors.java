/*
    Take N as input. Print all its factors

    ip : 6              ip : 24
    op : 1 2 3 6        op : 1 2 3 4 6 8 12 24
*/

class Solution {
    public static void main(String[] args) {

        int N = 24;

        for(int i = 1; i <= N; i++) {

            if(N % i == 0)
                System.out.print(i + "  ");
        }

        System.out.println();
    }
}