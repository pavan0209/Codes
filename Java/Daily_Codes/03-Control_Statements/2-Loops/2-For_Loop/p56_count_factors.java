/*
    Take N as input. Count all its factors and print count.

    ip : 6         ip : 24
    op : 4         op : 8
*/

class Solution {
    public static void main(String[] args) {

        int N = 24;
        int cnt = 0;

        for(int i = 1; i <= N; i++) {

            if(N % i == 0)
                cnt++;
        }

        System.out.println("Total Factors of " + N + " is :: " + cnt);
    }
}