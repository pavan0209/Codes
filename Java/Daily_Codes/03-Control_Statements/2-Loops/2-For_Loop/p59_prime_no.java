/*
    Take N as input. Print whether its prime or not

    ip : 5              ip : 8
    op : prime          op : Not Prime
*/

class Solution {
    public static void main(String[] args) {

        int N = 8;
        int cnt = 0;

        for(int i = 1; i <= N; i++) {
            
            if(N % i == 0)
                cnt++;
        }

        if(cnt == 2) {

            System.out.println("Prime");
        }
        else {
            System.out.println("Not Prime");
        }
    }
}