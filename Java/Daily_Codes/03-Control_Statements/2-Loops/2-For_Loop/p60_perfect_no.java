/*
    Take integer N as input. Print whether N is perfect number or not.
    
    ip : 4                          ip : 6
    op : Not a perfect Number       op : Perfect Number
*/

class Solution {
    public static void main(String[] args) {

        int N = 5;
        int sum = 0;

        for(int i = 1; i < N; i++) {

            if(N % i == 0) {

                sum = sum + i;
            }
        }

        if(sum == N) {
            System.out.println(N + " is a perfect number");
        }
        else {
            System.out.println(N + " is not a perfect number");
        }
    }
}
