/*
    Take N as input. Print whether the number is armstrong no or not.

    ip : 23                             ip : 153
    op : Not an armstrong number        op : Armstrong number
*/

class Solution {
    public static void main(String[] args) {

        int N = 13;
        int sum = 0;

        for(int i = N ; i != 0; i /= 10) {

            int rem = i % 10;
            sum += (rem * rem * rem);
        }

        if(sum == N) {
            System.out.println(N + " is an ARMSTRONG NUMBER");
        }
        else {
            System.out.println(N + " is Not an ARMSTRONG NUMBER");
        }
    }
}