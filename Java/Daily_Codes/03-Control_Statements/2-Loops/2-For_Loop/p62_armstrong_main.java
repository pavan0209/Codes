/*
    Take N as input. Print whether the number is armstrong no or not.

    ip : 23                             ip : 153
    op : Not an armstrong number        op : Armstrong number
*/

class Solution {
    public static void main(String[] args) {

        int N = 1634;
        int cnt = 0;
        int sum = 0;

        for(int i = N; i != 0; i /= 10)
            cnt++;

        for(int i = N; i != 0; i /= 10) {

            int rem = i % 10;
            int mult = 1;
            for(int j = 1; j <= cnt; j++) {

                mult = mult * rem;
            }
            sum += mult;
        }

        if(sum == N) {
            System.out.println(N + " is an ARMSTRONG NUMBER");
        }
        else {
            System.out.println(N + " is Not an ARMSTRONG NUMBER");
        }
    }
}