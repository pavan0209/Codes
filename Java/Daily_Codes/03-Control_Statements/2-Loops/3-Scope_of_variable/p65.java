/*
    Predict the output
*/

class Solution {
    public static void main(String[] args) {

        int x = 10;
        int y;

        {
            /*int*/ y = 20;
            System.out.println(x + " " + y);
        }
        {
            x = 15;
            System.out.println(x + " " + y);    //error : cannot find symbol
        }

        System.out.println(x + " " + y);    //error : cannot find symbol
    }
}