/*
    Prime number or not using break statement
*/

class Solution {
    public static void main(String[] args) {

        int N = 5;
        int cnt = 0;

        for(int i = 1; i <= N; i++) {
            
            if(N % i == 0)
                cnt++;
            
            if(cnt > 2)
                break;
        }

        if(cnt == 2) {

            System.out.println(N + " is a PRIME number");
        }
        else {

            System.out.println(N + " is not a PRIME number");
        }
    }
}