/*
    5  5  5  5
    5  5  5  5
    5  5  5  5
*/

class Solution {
    public static void main(String[] args) {

        System.out.println();

        for(int i = 1; i <= 3; i++) {

            for(int j = 1; j <= 4; j++) {

                System.out.print("5  ");
            }
            System.out.println();
        }
        System.out.println();
    }
}
