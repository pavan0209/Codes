/*
    7  7  7
    7  7  7
    7  7  7
    7  7  7
*/

class Solution {
    public static void main(String[] args) {

        System.out.println();

        for(int i = 1; i <= 4; i++) {

            for(int j = 1; j <= 3; j++) {

                System.out.print("7  ");
            }
            System.out.println();
        }
        System.out.println();
    }
}
