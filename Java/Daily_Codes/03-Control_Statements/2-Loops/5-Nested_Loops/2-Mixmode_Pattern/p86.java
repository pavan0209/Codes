/*
    *  *  *  *
    *        *
    *        *
    *  *  *  *
    
*/

class Solution {
    public static void main(String[] args) {

        System.out.println();
        int N = 4;

        for(int i = 1; i <= N; i++) {

            for(int j = 1; j <= N; j++) {

                if(i == 1 || j == 1 || j == N || i == N) {

                    System.out.print("*  ");
                }
                else {
                    System.out.print("   ");
                }
            }
            System.out.println();
        }
        System.out.println();
    }
}