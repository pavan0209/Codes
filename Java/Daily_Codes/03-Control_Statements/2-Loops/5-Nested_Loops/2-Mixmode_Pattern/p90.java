/*
    Print following pattern.

    10
    9  8
    7  6  5
    4  3  2  1
*/

class Solution {
    public static void main(String[] args) {

        System.out.println();
        int rows = 4;
        int num = (rows * rows + rows) / 2;

        for(int i = 1; i <= 4; i++) {

            for(int j = 1; j <= i; j++) {

                System.out.print(num-- + "  ");
            }
            System.out.println();
        }
        System.out.println();
    }
}