/*
    Print following pattern.

    *  *  *  *
    *  *  *
    *  *
    *
*/

class Solution {
    public static void main(String[] args) {

        System.out.println();
        int rows = 4;

        for(int i = 1; i <= rows; i++) {

            for(int j = 1; j <= rows-i+1; j++) {

                System.out.print("*  ");
            }
            System.out.println();
        }
        System.out.println();
    }
}