/*
    Print following pattern.

    A  B  C  D
    A  B  C
    A  B
    A
*/

class Solution {
    public static void main(String[] args) {

        System.out.println();

        int rows = 4;

        for(int i = 1; i <= rows; i++) {

            for(int j = 1; j <= rows-i+1; j++) {

                System.out.print((char)(64+j) + "  ");
            }
            System.out.println();
        }
        System.out.println();
    }
}