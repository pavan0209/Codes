/*
    Print following pattern.

    A  1  B  2
    C  3  D
    E  4
    F
*/

class Solution {
    public static void main(String[] args) {

        System.out.println();
        char ch = 'A';
        int num = 1;

        int rows = 4;

        for(int i = 1; i <= rows; i++) {

            for(int j = 1; j <= rows-i+1; j++) {

                if(j % 2 != 0) {
                    System.out.print(ch++ + "  ");
                }
                else {
                    System.out.print(num++ + "  ");
                }
            }
            System.out.println();
        }
        System.out.println();
    }
}