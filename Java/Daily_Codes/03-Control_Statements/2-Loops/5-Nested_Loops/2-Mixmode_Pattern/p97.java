/*
    Print following pattern.

    1
    2  c
    4  e  6
    7  h  9  j
*/

class Solution {
    public static void main(String[] args) {

        System.out.println();
        char ch = 'a';
        int num = 1;

        int rows = 4;

        for(int i = 1; i <= rows; i++) {

            for(int j = 1; j <= i; j++) {

                if(j % 2 == 0) {
                    System.out.print(ch + "  ");
                }
                else {
                    System.out.print(num + "  ");
                }
                ch++;
                num++;
            }
            System.out.println();
        }
        System.out.println();
    }
}