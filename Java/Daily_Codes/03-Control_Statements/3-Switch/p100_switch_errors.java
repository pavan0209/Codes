
/*
    Switch statement in JAVA.

*/

class Solution {
    public static void main(String[] args) {

        int x = 2;

        System.out.println();

        switch (x) {

            default:
                    System.out.println("No Match");
                    break;
            case 1:
                    System.out.println("first one");
                    break;
            case 2:
                    System.out.println("first two");
                    break;

            case 1:     //error: duplicate case label
                    System.out.println("second one");
                    break;

            case 2:     //error: duplicate case label
                    System.out.println("second two");
                    break;

        }
        System.out.println("After Switch");

        System.out.println();
    }
}
