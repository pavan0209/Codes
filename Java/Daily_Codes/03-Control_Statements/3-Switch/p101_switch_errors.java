
/*
    Switch statement in JAVA.
    Errors while using switch case

*/

class Solution {
    public static void main(String[] args) {

        int x = 2;

        System.out.println();

        switch (x) {

            case 1:
                    System.out.println("one");
                    break;

            case 1+1:
                    System.out.println("two");
                    break;

            case 1+2:     
                    System.out.println("second one");
                    break;

            case 5:     
                    System.out.println("second two");
                    break;

           /*  case 2:     //error: duplicate case label
                    System.out.println("two");
                    break;*/

            default:
                    System.out.println("No Match");
                    break;

        }
        System.out.println("After Switch");

        System.out.println();
    }
}