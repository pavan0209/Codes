
/*
    Switch statement in JAVA.

*/

class Solution {
    public static void main(String[] args) {

        int x = 65;

        System.out.println();

        switch (x) {

            
            case 'A':
                    System.out.println("char-A");
                    break;

            case 65:      //error: duplicate case label
                    System.out.println("num-65");
                    break;

            case 'B':
                    System.out.println("char-B");
                    break;

           case 66:    //error: duplicate case label
                    System.out.println("num-66");
                    break;

            default:
                    System.out.println("No Match");
                    break;  

        }
        System.out.println("After Switch");

        System.out.println();
    }
}
