
/*
    Switch statement in JAVA.

*/

class Solution {
    public static void main(String[] args) {

        int x = 3;
        int a = 1;
        int b = 2;
        System.out.println(a);
        System.out.println(b);

        System.out.println();

        switch (x) {

            
           case a:     //error: constant expression required
                    System.out.println("1");
                    break;

            case b:     //error: constant expression required
                    System.out.println("2");
                    break;

            case a+b:   //error: constant expression required
                    System.out.println("3");
                    break;

           case a+a+b:    //error: constant expression required   
                    System.out.println("4");
                    break;

            case a+b+b:   //error: constant expression required      
                    System.out.println("5");
                    break;

            default:
                    System.out.println("Invalid");
                    break;  

        }
        System.out.println("After Switch");

        System.out.println();
    }
}
