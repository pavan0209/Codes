
/*
    Switch statement in JAVA.

*/

class Solution {
    public static void main(String[] args) {

        String str = "Mon";

        System.out.println();

        switch (str) {

            
            case "Mon":
                    System.out.println("Monday");
                    break;

            case "Tue":
                    System.out.println("Tuesday");
                    break;

            default:
                    System.out.println("It's Sunday");
                    break;  

        }

        System.out.println();
    }
}