
/*
    Switch statement in JAVA.

*/

class Solution {
    public static void main(String[] args) {

        String str = "Non-Veg";

        System.out.println();

        switch (str) {

            
            case "Non-Veg": {

                                String str1 = "MainCourse";

                                switch(str1) {

                                    case "Starters" :
                                                    System.out.println("Chicken Tikka");
                                                    break;

                                    case "MainCourse":
                                                    System.out.println("Chicken Thali");
                                                    break;
                                }
                                break;
            }
            case "Veg": {

                                String str1 = "Starter";

                                switch(str1) {

                                    case "Starters" :
                                                    System.out.println("Paneer Tikka");
                                                    break;

                                    case "MainCourse":
                                                    System.out.println("Butter Paneer");
                                                    break;
                                }
                                break;
            }           
            default:
                    System.out.println("Ghari Jaa");
                    break;  

        }

        System.out.println();
    }
}