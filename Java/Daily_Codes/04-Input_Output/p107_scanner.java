/*
    Input output in java
*/

import java.util.Scanner;

class Solution {
    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in); 

        System.out.print("\nEnter your Name :: ");
        String name = sc.next();
        
        System.out.println("Name :: " + name);

        sc.close();
    }
}