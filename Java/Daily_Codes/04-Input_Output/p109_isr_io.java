/*
    InputStreamReader can read only one character from keyboard
*/

/*import java.io.IOException;
import java.io.InputStreamReader;
*/
import java.io.*;

class Solution {
    public static void main(String[] args) throws IOException{

        InputStreamReader isr = new InputStreamReader(System.in);

        System.out.print("Enter character:: ");
        //char ch = isr.read();           //error: incompatible types: possible lossy conversion from int to char

        isr.close();
    }
}