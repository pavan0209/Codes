/*
    InputStreamReader can read only one character from keyboard
*/

import java.io.*;

class Solution {
    public static void main(String[] args) throws IOException{

        InputStreamReader isr = new InputStreamReader(System.in);

        System.out.print("Enter character:: ");
        char ch = (char) isr.read();                //(char) :: Explicit Typecasting      

        System.out.println("Character :: " + ch);

        //isr.close();
    }
}