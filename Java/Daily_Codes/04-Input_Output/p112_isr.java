/*
    InputStreamReader can read only one character from keyboard
*/

import java.io.*;

class Solution {
    public static void main(String[] args) throws IOException{

        InputStreamReader isr = new InputStreamReader(System.in);

        System.out.print("Enter character:: ");
        char ch = (char) isr.read();                //(char) :: Explicit Typecasting   

        isr.skip(1);        //Skip 1 character present in buffer

        isr.close();     

        System.out.print("Enter character:: "); //Runtime Error: Exception in thread "main" java.io.IOException: Stream closed
        int ch1 = isr.read();
        
        System.out.println("\nCharacter :: " + ch);
        System.out.println("Character :: " + ch1);

        //isr.close();
    }
}