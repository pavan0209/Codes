/*
    BufferedReader class is used for taking input in Java.
*/

import java.io.*;

class Solution {
    public static void main(String[] args) throws IOException {

        InputStreamReader isr = new InputStreamReader(System.in);
        BufferedReader br = new BufferedReader(isr);

        System.out.print("Enter Name:: ");
        String name = br.readLine();

        System.out.print("Enter Age :: ");
        //int age = br.readLine();            //error: incompatible types: String cannot be converted to int

        int age = Integer.parseInt(br.readLine());      //Integer : Wrapper Class

        System.out.println("\nName:: " + name);
        System.out.println("Age:: " + age);

        br.close();
    }
}