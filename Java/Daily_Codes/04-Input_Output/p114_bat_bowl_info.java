/*
    Take Batsman name and Bowler name from user and print Batsman and Bowler name.
*/

import java.io.*;

class Solution {
    public static void main(String[] args) throws IOException {

        /*InputStreamReader isr = new InputStreamReader(System.in);
        BufferedReader br = new BufferedReader(isr);*/

        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

        System.out.print("Enter Batsman Name:: ");
        String batsmanName = br.readLine();

        System.out.print("Enter Bowler Name :: ");
        String bowlerName = br.readLine();    

        System.out.println("\nBatsman Name:: " + batsmanName);
        System.out.println("Bowler Name:: " + bowlerName);

        br.close();
    }
}