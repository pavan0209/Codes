/*
    Take Information related to Player(name, jerseyNo, avg) and print information.
*/

import java.io.*;

class Solution {
    public static void main(String[] args) throws IOException {

        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

        System.out.print("Enter Batsman Name:: ");
        String batsmanName = br.readLine();

        System.out.print("Enter Jersey No. :: ");
        int jerseyNo = Integer.parseInt(br.readLine());    

        System.out.print("Enter Average :: ");
        float avg = Float.parseFloat(br.readLine());    

        System.out.println("\nBatsman Name :: " + batsmanName);
        System.out.println("Jersey No. :: " + jerseyNo);
        System.out.println("Average :: " + avg);

        br.close();
    }
}