/*
    Take Information related to Society(name, Wing, flatNo) and print information.
*/

import java.io.*;

class Solution {
    public static void main(String[] args) throws IOException {

        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

        System.out.print("Enter Society Name:: ");
        String sName = br.readLine();

        System.out.print("Enter Wing :: ");
        char sWing = (char)br.read();    

        //br.read();   to take dummy character \n which is present in buffer
        br.skip(1);     //Skips one character which is '\n' 

        System.out.print("Enter FlatNo :: ");       //Runtime Error: Exception in thread "main" java.lang.NumberFormatException: For input string: ""
        int flatNo = Integer.parseInt(br.readLine());    

        System.out.println("\nSociety Name :: " + sName);
        System.out.println("Wing :: " + sWing);
        System.out.println("Flat No. :: " + flatNo);

        br.close();
    }
}