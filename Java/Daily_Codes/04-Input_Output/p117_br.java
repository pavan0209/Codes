/*

*/

import java.io.*;

class Solution {
    public static void main(String[] args) throws IOException {

        BufferedReader br1 = new BufferedReader(new InputStreamReader(System.in));
        BufferedReader br2 = new BufferedReader(new InputStreamReader(System.in));

        System.out.print("Enter Society Name:: ");
        String sName1 = br1.readLine();

        br1.close();    //if connection is close then all other objects of BufferedReader are also stopped

        System.out.print("Enter Society Name :: ");     //Runtime : Exception in thread "main" java.io.IOException: Stream closed
        String SName2 = br2.readLine();    

        System.out.println("\nSociety Name :: " + sName1);
        System.out.println("Society Name :: " + SName2);

        
    }
}