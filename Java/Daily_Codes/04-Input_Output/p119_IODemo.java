/*
 
*/

import java.io.*;

class Solution {
    public static void main(String[] args) throws IOException{

        InputStreamReader isr = new InputStreamReader(System.in);
        BufferedReader br = new BufferedReader(isr);

        String str1 = br.readLine();
        System.out.println("Str1 :: " + str1);

        br.close();

        char ch = (char)br.read();      //Exception in thread "main" java.io.IOException: Stream closed
        System.out.println("character :: " + ch);

    }
}
