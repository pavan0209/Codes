/*
    Take MatchInfo, ManOfMatch and Runs in a single readLine() method
    
    StringTokenizer is used to separate string
*/

import java.io.*;
import java.util.*;

class Solution {
    public static void main(String[] args) throws IOException {

        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

        System.out.print("Enter MatchInfo, ManOfMatch, Runs :: ");
        String str = br.readLine();

        StringTokenizer st = new StringTokenizer(str);

        String str1 = st.nextToken();
        String str2 = st.nextToken();
        String str3 = st.nextToken();

        System.out.println("\nMatch Info :: " + str1);
        System.out.println("Man of the Match :: " + str2);
        System.out.println("Runs by MOM :: " + str3);

        br.close();
    }
}