/*
    Take PlayerName, Grade, Runs and Average in a single readLine() method
    
    StringTokenizer is used to separate string
*/

import java.io.*;
import java.util.*;

class Solution {
    public static void main(String[] args) throws IOException {

        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

        System.out.print("Enter PlayerName, Grade Of Player, Runs and Average :: ");
        String str = br.readLine();

        StringTokenizer st = new StringTokenizer(str, ",");

        String pName = st.nextToken();
        char grade = st.nextToken().charAt(0);
        int runs = Integer.parseInt(st.nextToken());
        float avg = Float.parseFloat(st.nextToken());

        System.out.println("\nPlayer Name :: " + pName);
        System.out.println("Grade of player :: " + grade);
        System.out.println("Runs :: " + runs);
        System.out.println("Average :: " + avg);

        br.close();
    }
}