/*
    Take input using Scanner class in a single readLine() and print all data separately
*/

import java.util.*;

class Solution {
    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);

        System.out.print("Enter Player info :: ");
        String str = sc.nextLine();

        StringTokenizer st = new StringTokenizer(str, " ");

        System.out.println("Total Tokens :: " + st.countTokens());

        while(st.hasMoreTokens()) {

            System.out.println(st.nextToken());
        }

        System.out.println();

        sc.close();
    }
}