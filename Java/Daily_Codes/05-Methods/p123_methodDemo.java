/*
    Methods in java are of two types
        1. Static Methods (Methods starting with static keyword)
        2. Non Static Methods
*/

class Solution {
    public static void main(String[] args) {

        //fun();     //error: non-static method fun() cannot be referenced from a static context
    }

    void fun() {

        System.out.println("In Fun Method");
    }
}