/*
    Methods in java are of two types
        1. Static Methods (Methods starting with static keyword)
        2. Non Static Methods
*/

class Solution {
    public static void main(String[] args) {
        
        gun();
        fun();     //error: non-static method fun() cannot be referenced from a static context
    }

    static void gun () {

        System.out.println("In Gun Method");
    }

    void fun() {

        System.out.println("In Fun Method");
    }
}
