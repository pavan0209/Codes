/*
    Calling Static Method From Static Method
*/

class Solution {
    
    static void gun () {

        System.out.println("In Gun Method");
    }
    
    public static void main(String[] args) {
        
        System.out.println("In Main");
        gun();
        System.out.println("End Main");
    }
}