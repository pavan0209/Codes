/*
    Accessing Static Method From Static Method
              Non-Static Method from Static Method
              Static Variable from Static Method
              Non-Static Variable from Static Method

    Note :
        When we Access non-static things from static methods we should have to create object for accessing.
        We can access static things using object as well.
*/

class Solution {

    int x = 10;
    static int y = 20;
    
    static void gun() {

        System.out.println("In Gun Method");
    }

    void fun() {

        System.out.println("In Fun Method");
    }
    public static void main(String[] args) {
        
        Solution s1 = new Solution();       //Object creation

        System.out.println("In Main");
        System.out.println("x :: " + s1.x);
        System.out.println("y :: " + s1.y);
        
        s1.fun();
        s1.gun();
        
        System.out.println("End Main");
    }
}