/*

*/

class Solution {

    int x = 10;
    static int y = 20;
    
    void fun() {        //We can access both static and non-static things in non-static method directly

        System.out.println("In Fun Method");    
        System.out.println("x :: " + x);
        System.out.println("y :: " + y);
        System.out.println("End Fun Method");
    }
    public static void main(String[] args) {    //We cannot access both static and non-static things in non-static method directly
                                                //to access we need to create object of class
        
        Solution s1 = new Solution();

        System.out.println("In Main");
        System.out.println("x :: " + s1.x);
        System.out.println("y :: " + y);
        
        s1.fun();
        
        System.out.println("End Main");
    }
}