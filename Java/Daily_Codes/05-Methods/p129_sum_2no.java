/*
    Write a program to take 2 values from a user and print their sum without using a function
*/

import java.util.*;

class Solution {
    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);

        System.out.printf("Enter 1st value :: ");
        int n1 = sc.nextInt();

        System.out.printf("Enter 2nd value :: ");
        int n2 = sc.nextInt();

        int ans = n1 + n2;

        System.out.println("Sum :: " + ans);

        sc.close();
    }
}