/*
    **Passing Arguments to Method **

    Write a program to take 2 values from a user and print their sum and pass values to Method/Function
*/

import java.util.*;

class Solution {

    static void Sum(int a, int b) {     // a, b => Parameters

        int ans = a + b;
        System.out.println("Addition :: " + ans);

    }   
    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);

        System.out.printf("Enter 1st value :: ");
        int n1 = sc.nextInt();

        System.out.printf("Enter 2nd value :: ");
        int n2 = sc.nextInt();

        Sum(n1, n2);    // n1, n2 => arguments
                        //Sum() => Method Call
        sc.close();
    }
}