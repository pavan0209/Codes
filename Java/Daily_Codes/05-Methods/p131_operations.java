/*
    **Passing Arguments to Method **

    Write a program to take 2 values from a user and print their addition, subtraction, multiplication
     Division using Methods
*/

import java.util.*;

class Solution {

    static void Add(int a, int b) { 

        int ans = a + b;
        System.out.println("Addition :: " + ans);

    }   
    static void Sub(int a, int b) { 

        int ans = a - b;
        System.out.println("Subtraction :: " + ans);

    } 
    static void Mult(int a, int b) { 

        int ans = a * b;
        System.out.println("Multiplication :: " + ans);

    } 
    static void Div(int a, int b) { 

        int ans = a / b;
        System.out.println("Division :: " + ans);

    } 
    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);

        System.out.printf("Enter 1st value :: ");
        int n1 = sc.nextInt();

        System.out.printf("Enter 2nd value :: ");
        int n2 = sc.nextInt();

        Add(n1, n2);  
        Sub(n1, n2);
        Mult(n1, n2);
        Div(n1, n2); 

        sc.close();
    }
}