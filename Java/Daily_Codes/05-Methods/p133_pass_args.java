/*
    Passing Arguments to a function
*/

class Solution {
    
    void fun() {

        System.out.println("In Fun Method");
        System.out.println("x :: " + x);        //error : error: cannot find symbol
        System.out.println("End Fun Method");
    }
    public static void main(String[] args) {

        Solution obj = new Solution();

        obj.fun(10);        //error: method fun in class Solution cannot be applied to given types;
                            // obj.fun(10);
                            //    ^
                            // required: no arguments
                            // found:    int
                            // reason: actual and formal argument lists differ in length

    }
}