/*
    Passing Arguments to a function
*/

class Solution {
    
    void fun(int x) {

        System.out.println("In Fun Method");
        System.out.println("x :: " + x);       
        System.out.println("End Fun Method");
    }
    public static void main(String[] args) {

        Solution obj = new Solution();

        obj.fun();          //error: method fun in class Solution cannot be applied to given types;
                            //         obj.fun();        
                            //            ^
                            // required: int
                            // found:    no arguments
                            // reason: actual and formal argument lists differ in length

    }
}