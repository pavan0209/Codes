/*
    Passing Arguments to a function
*/

class Solution {
    
    void fun(int x) {

        System.out.println("In Fun Method");
        System.out.println("x :: " + x);       
        System.out.println("End Fun Method");
    }
    public static void main(String[] args) {

        Solution obj = new Solution();

        obj.fun(10);       
        obj.fun(10.5f);       //error: incompatible types: possible lossy conversion from float to int
                                //         obj.fun(10.5f);
                                //                 ^
                                // Note: Some messages have been simplified; recompile with -Xdiags:verbose to get full output
    }
}