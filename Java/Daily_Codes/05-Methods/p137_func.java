/*
    Passing Arguments to a function
*/

class Solution {
    
    void fun(float x) {

        System.out.println("In Fun Method");
        System.out.println("x :: " + x);       
    }
    public static void main(String[] args) {

        Solution obj = new Solution();

        obj.fun(10);       
        obj.fun(10.5f); 
        obj.fun('A');      
        
    }
}