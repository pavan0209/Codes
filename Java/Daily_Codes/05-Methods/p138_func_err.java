/*
    Passing Arguments to a function
*/

class Solution {
    
    void fun(float x) {

        System.out.println("In Fun Method");
        System.out.println("x :: " + x);       
    }
    public static void main(String[] args) {

        Solution obj = new Solution();

        obj.fun(10);       
        obj.fun(10.5f); 
        obj.fun('A');      
        obj.fun(10.5);      //error: incompatible types: possible lossy conversion from double to float
        obj.fun(true);      //error: incompatible types: boolean cannot be converted to float
    }
}