/*
    Returning Values from Method
*/

class Solution {
    
    int fun(int x) {

       return x + 10;      
    }
    public static void main(String[] args) {

        Solution obj = new Solution();

        System.out.println(obj.fun(10));       
    }
}