/*
    Returning Values from Method
*/

class Solution {
   
   //void fun(int x){ 
    int fun(int x) {
        
        int y = x + 10;
        return y;
    }
    
    public static void main(String[] args) {

        Solution obj = new Solution();

        //System.out.println(obj.fun(10));    //error: 'void' type not allowed here    
        System.out.println(obj.fun(10));
    }
}
