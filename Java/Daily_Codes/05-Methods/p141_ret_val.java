/*
    Returning Values from Method
*/

class Solution {
    
    int fun(int x) {
        
        return x + 10;
    }
    public static void main(String[] args) {

        Solution obj = new Solution();

        int retVal = obj.fun(10);
        System.out.println("return value :: " + retVal);
    }
}