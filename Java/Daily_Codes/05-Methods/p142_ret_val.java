/*
    Returning Values from Method
*/

class Solution {
    
    void fun(int x) {
        
        int y =  x + 10;
    }
    public static void main(String[] args) {

        Solution obj = new Solution();

        int retVal = obj.fun(10);   //error: incompatible types: void cannot be converted to int

        System.out.println("return value :: " + retVal);    
    }
}