/*
    Local and Global Variable in java
*/

class Solution {

    int x = 10;     //Global Variable

    public static void main(String[] args) {

        int y = 20;     //Local Variable

        System.out.println("a :: " + x);    //error: non-static variable x cannot be referenced from a static context
        System.out.println("b :: " + y);
    }
}
