/*
    Array Declaration and initialization in JAVA.
*/

class Solution {
    public static void main(String[] args) {

        int arr1[];
        int arr2[] = new int[5];
        int arr[] = new int[];      //error: array dimension missing

    }
}
