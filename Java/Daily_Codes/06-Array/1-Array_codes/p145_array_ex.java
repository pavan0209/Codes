/*
    Array Example.
*/

class Solution {
    public static void main(String[] args) {

        int arr1[] = new int[] {10, 20, 30, 40, 50};
        int arr2[] = new int[4];

        arr2[0] = 10;
        arr2[3] = 40;

        System.out.println("arr1[0] :: " + arr1[0]);
        System.out.println("arr2[0] :: " + arr2[0]);
        System.out.println("arr2[1] :: " + arr2[1]);
    }
}