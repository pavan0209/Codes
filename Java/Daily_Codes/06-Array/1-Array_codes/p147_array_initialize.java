/*
    Different ways to initialize array.
*/

import java.util.*;

class Solution {
    public static void main(String[] args) {

        int arr1[] = new int[] {10, 20, 30, 40, 50};
        int arr2[] = {10, 20, 30, 40, 50};

        System.out.println("\narr1 :: " + Arrays.toString(arr1));
        System.out.println("arr2 :: " + Arrays.toString(arr2) + "\n");
    }
}