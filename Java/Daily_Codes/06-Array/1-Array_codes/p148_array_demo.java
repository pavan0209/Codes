/*
    Different ways to initialize array.
*/

import java.util.*;

class Solution {
    public static void main(String[] args) {

        int arr1[] = new int[4];

        arr1[0] = 10;
        arr1[1] = 10;
        arr1[2] = 10;
        arr1[3] = 10;

        int arr2[] = {20, 20, 20, 20};

        int arr3[] = new int[] {30, 30, 30, 30};

        int arr4[] = new int[4] {40, 40, 40, 40};      //error: array creation with both dimension expression and initialization is illegal
    
        System.out.println("\narr1 :: " + Arrays.toString(arr1));
        System.out.println("arr2 :: " + Arrays.toString(arr2));
        System.out.println("arr3 :: " + Arrays.toString(arr3) + "\n");
    }
}
