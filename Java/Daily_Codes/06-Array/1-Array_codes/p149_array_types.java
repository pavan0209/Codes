/*
    Different types of array in java
        1. int
        2. char
        3. float
        4. boolean
        5. string
        6. long
        7. double
        8. objects
*/
class Solution {
    public static void main(String[] args) {

        int arr1[] = {10, 20, 30, 40, 50};      //Integer Array
        char arr2[] = {'A', 'B', 'C'};      //Character Array
        float arr3[] = {10.25f, 20.5f};     //Float Array
        boolean arr4[] = {true, false, true};   // Boolean array

        //Printing Integer array
        System.out.println("\narr1[0] :: " + arr1[0]);
        System.out.println("arr1[1] :: " + arr1[1]);
        System.out.println("arr1[2] :: " + arr1[2]);
        System.out.println("arr1[3] :: " + arr1[3]);
        System.out.println("arr1[4] :: " + arr1[4]);

        //Printing character array
        System.out.println("\narr2[0] :: " + arr2[0]);
        System.out.println("arr2[1] :: " + arr2[1]);
        System.out.println("arr2[2] :: " + arr2[2]);

        //Printing float array
        System.out.println("\narr3[0] :: " + arr3[0]);
        System.out.println("arr3[1] :: " + arr3[1]);

        //Printing boolean array
        System.out.println("\narr4[0] :: " + arr4[0]);
        System.out.println("arr4[1] :: " + arr4[1]);
        System.out.println("arr4[2] :: " + arr4[2]);

        System.out.println();
    }   
}