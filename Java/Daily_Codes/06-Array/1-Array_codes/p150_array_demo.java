/*
    Example of ArrayIndexOutOfBoundsException
*/

class Solution {
    public static void main(String[] args) {

        float arr[] = {10.5f, 20.5f};

        System.out.println("arr[0] :: " + arr[0]);
        System.out.println("arr[1] :: " + arr[1]);
        System.out.println("arr[2] :: " + arr[2]);  //Exception in thread "main" java.lang.ArrayIndexOutOfBoundsException: 
                                                    //Index 2 out of bounds for length 2
    }
}