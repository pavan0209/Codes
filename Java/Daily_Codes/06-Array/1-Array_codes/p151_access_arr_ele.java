/*
    Accessing array elements 
*/

class Solution {
    public static void main(String[] args) {

        int arr[] =  {10, 20, 30, 40, 50};

        System.out.println("arr[0] :: " + arr[0]);
        System.out.println("arr[1] :: " + arr[1]);
        System.out.println("arr[2] :: " + arr[2]);
        System.out.println("arr[3] :: " + arr[3]);
        System.out.println("arr[4] :: " + arr[4]);

        System.out.println();

        //Printing array elements using for loop
 
        for(int i = 0; i < 5; i++) {

            System.out.println("arr[" + i + "] :: " + arr[i]);
        }

        System.out.println();
    }
}
