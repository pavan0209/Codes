/*
    Taking array elements from user
*/

import java.io.*;

class Solution {
    public static void main(String[] args) throws IOException {

        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

        int arr[] = new int[5];

        System.out.println("Enter array elements : ");
        for(int i = 0; i < 5; i++) {

            arr[i] = Integer.parseInt(br.readLine());
        }
        System.out.print("Array elements are :: ");

        for(int i = 0; i < 5; i++) {

            System.out.print(arr[i] + "  ");
        }
        System.out.println("\n");
    }
}