/*
    Sum Of array elements.
*/

import java.io.*;

class Solution {
    public static void main(String[] args) throws IOException {

        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

        int arr[] = new int[5];
        int sum = 0;

        System.out.println("Enter array elements : ");
        for(int i = 0; i < 5; i++) {

            arr[i] = Integer.parseInt(br.readLine());
            sum += arr[i];
        }
        
        System.out.println("Sum of Array elements is :: " + sum);
        System.out.println();
    }
}