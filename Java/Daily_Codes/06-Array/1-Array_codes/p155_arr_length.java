/*
    In JAVA we can find array length by using arr.length
*/

import java.io.*;

class Solution {
    public static void main(String[] args) throws IOException {

        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

        int arr[] = new int[5];
        int sum = 0;

        System.out.println("Enter array elements : ");
        for(int i = 0; i < arr.length; i++) {

            arr[i] = Integer.parseInt(br.readLine());
            sum += arr[i];
        }
        
        System.out.println("\nSum of Array elements is :: " + sum);
        System.out.println("Length of array :: " + arr.length);
        //System.out.println("Length of array :: " + arr.length());   //error: cannot find symbol
        
        System.out.println();
    }
}