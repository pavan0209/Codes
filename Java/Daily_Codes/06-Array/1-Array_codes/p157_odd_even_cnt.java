/*
    WAP to find all even numbers count and odd numbers count in array.
*/

import java.io.*;

class Solution {
    public static void main(String[] args) throws IOException {

        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

        System.out.print("\nEnter size for array :: ");
        int size = Integer.parseInt(br.readLine());

        int arr[] = new int[size];
        int cnt = 0;

        System.out.println("Enter array elements : ");
        for(int i = 0; i < arr.length; i++) {

            arr[i] = Integer.parseInt(br.readLine());
            
            if(arr[i] % 2 == 0) {
                cnt++;
            }
        }
        
        System.out.println("\nEven numbers count in array :: " + cnt);
        System.out.println("Odd numbers count in array :: " + (size-cnt));
        
        System.out.println();
    }
}