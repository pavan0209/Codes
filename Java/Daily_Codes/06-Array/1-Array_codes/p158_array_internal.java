/*
    Internals of array.
*/

import java.io.*;

class Solution {
    public static void main(String[] args) throws IOException {

        int arr1[] = {10, 20, 30, 40, 40};

        System.out.println("arr1[0] :: " + arr1[0]);
        System.out.println("arr1 :: " + arr1);          // [I@14dad5dc

        int arr2[] = {10, 20, 30, 40, 50};

        System.out.println("arr2[0] :: " + arr2[0]);
        System.out.println("arr2 :: " + arr2);          // [I@18b4aac2

        System.out.println();
    }
}