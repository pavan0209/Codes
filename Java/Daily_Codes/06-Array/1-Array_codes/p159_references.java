/*
    Internals of array. In java we can find a pointer alike value called reference by using 
        System.identityHashCode() method.
*/

import java.io.*;

class Solution {
    public static void main(String[] args) throws IOException {

        int arr1[] = {10, 20, 30, 40, 40};
        int arr2[] = {10, 20, 30, 40, 50};

        System.out.println("arr1[0] :: " + arr1[0]);
        System.out.println("arr2[0] :: " + arr2[0]);
       
        System.out.println("\narr1 :: " + arr1);          // [I@14dad5dc
        System.out.println("arr2 :: " + arr2);          // [I@18b4aac2

        System.out.println("\nIdHC(arr1[0]) :: " + System.identityHashCode(arr1[0]));   // 1461863265
        System.out.println("IdHC(arr2[0]) :: " + System.identityHashCode(arr2[0]));     // 1461863265
                                                                                //Gives same identityHashCode
                                                                                //because of integer cache.

        System.out.println();
    }
}