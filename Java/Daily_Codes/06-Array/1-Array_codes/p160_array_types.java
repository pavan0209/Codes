/*
    Different types of array in java.

*/

class Solution {
    public static void main(String[] args) {

        int arr1[] = {100, 200, 300, 400, 500};
        byte arr2[] = {1, 2, 3, 4, 5};
        short arr3[] = {10, 20, 30, 40, 50};
        boolean arr4[] = {true, false, true};
        float arr5[] = {10.5, 20.5, 30.5};      // error: incompatible types: possible lossy conversion
                                                //from doublr to float.
        float arr6[] = {10.5f, 20.5f, 30.5f};
        double arr7[] = {10.5, 20.5, 30.5};
        double arr8[] = {10d, 20d, 30d, 40d};
        char arr9[] = {'A', 'B', 'C'};

        char arr10[] = new char[5];
        String str = "Pavan";       //internally uses character array.

        System.out.println("\narr1 :: " + arr1);
        System.out.println("arr2 :: " + arr2);
        System.out.println("arr3 :: " + arr3);
        System.out.println("arr4 :: " + arr4);
        System.out.println("arr5 :: " + arr5);
        System.out.println("arr6 :: " + arr6);
        System.out.println("arr7 :: " + arr7);
        System.out.println("arr8 :: " + arr8);
        System.out.println("arr9 :: " + arr9);
        System.out.println("arr10 :: " + arr10);
        System.out.println("String :: " + str);
        
        System.out.println();
    }
}
