/*
    Integer cache in JAVA.
*/

class Solution {
    public static void main(String[] args) {

        int x = 10;
        int y = 20;

        System.out.println("\nIdHC(x) :: " + System.identityHashCode(x));   // 1461863265
        System.out.println("IdHC(y) :: " + System.identityHashCode(y));     // 636634632
       
        int a = 10;
        int b = 10;

        System.out.println("\nIdHC(a) :: " + System.identityHashCode(a));   // 1461863265
        System.out.println("IdHC(b) :: " + System.identityHashCode(b));     // 1461863265

        int p = 10; 
        int q = 10;
        Integer r = 10;

        System.out.println("\nIdHC(p) :: " + System.identityHashCode(p));   // 1461863265
        System.out.println("IdHC(q) :: " + System.identityHashCode(q));     // 1461863265
        System.out.println("\nIdHC(r) :: " + System.identityHashCode(r));   // 1461863265

        System.out.println();

    }
}