/*
    identityHashCode().
*/

class Solution {
    public static void main(String[] args) {

        int x = 10;
        int y = 10;

        if(System.identityHashCode(x) == System.identityHashCode(y)) {

            System.out.println("\nSame/true\n");
        }
        else {

            System.out.println("\nDifferent/false\n");
        }
    }
}