/*
    Integer Cache was come in JAVA 1.5 version. Integer cache is only for integer values
*/

class Solution {
    public static void main(String[] args) {

        float f = 10.5f;
        float x = 10.5f;

        System.out.println("\nIdHC(f) :: " + System.identityHashCode(f));   // 2060468723
        System.out.println("IdHC(x) :: " + System.identityHashCode(x));     // 349885916
    }
}