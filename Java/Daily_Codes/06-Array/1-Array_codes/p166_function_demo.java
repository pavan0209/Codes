/*
    Simple Function program
*/

class Solution {
    public static void main(String[] args) {

        Solution s1 = new Solution();

        fun();      //error: non-static method fun() cannot be referenced from a static context

        s1.fun();
        System.out.println();
    }

    void fun() {

        System.out.println("\nIn Fun");
    }
}
