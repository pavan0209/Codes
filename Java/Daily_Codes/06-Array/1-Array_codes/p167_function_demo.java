/*
    Simple Function program
*/

class Solution {
    public static void main(String[] args) {

        Solution s1 = new Solution();
        s1.fun();
        //s1.fun(10);     // error: method fun in class Solution cannot be applied to given types;
                        //required: no arguments
                        //found:    int
                        //reason: actual and formal argument lists differ in length
        
        System.out.println();
    }

    void fun() {

        System.out.println("\nIn Fun");
    }
}