/*
    Returning Values from function.
*/


class Solution {
    public static void main(String[] args) {

        Solution s1 = new Solution();
        s1.fun(10);
    }

    void fun(int x) {

        int val = x + 10;
        System.out.println(val);
        //return val;     //error: incompatible types: unexpected return value
    }
}