/*
    Returning Values from function.
*/


class Solution {
    public static void main(String[] args) {

        Solution s1 = new Solution();
        int retVal = s1.fun(10);

        System.out.println("return value:: " + retVal);
    }

    int fun(int x) {

        int val = x + 10;

        return val;  
    }
}