/*
    Passing arguments to a function
*/

class Solution {

    static void fun(int x, double y) {

        System.out.println("\nx:: " + x);
        System.out.println("y :: " + y);
    }

    public static void main(String[] args) {

        fun(10, 20.5);

        System.out.println(fun(10, 20.5);     // error : 'void' type not allowed here.
    }
}
