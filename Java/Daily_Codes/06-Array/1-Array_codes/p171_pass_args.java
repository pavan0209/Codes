/*
    Passing arguments to a function
*/

class Solution {

    static void fun(int x, double y) {

        System.out.println("\nx:: " + x);
        System.out.println("y :: " + y + "\n");
    }

    public static void main(String[] args) {

        fun('A', 20);

    }
}