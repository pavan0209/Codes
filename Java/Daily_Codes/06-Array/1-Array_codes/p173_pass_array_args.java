/*
    Passing array as an argument to a method.
*/


class Solution {

    static void fun(int xarr[]) {

        System.out.println("\nStart fun");
        System.out.print("Array elements :: ");

        for(int x : xarr) {

            System.out.print(x + "  ");
        }

        xarr[0] = 50;
        System.out.println("\nEnd Fun\n");
    }

    public static void main(String[] args) {

        int arr[] = {10, 20, 30};

        System.out.print("\nArray elements :: ");
        for(int x : arr) {

            System.out.print(x + "  ");
        }

        fun(arr);

        System.out.print("Array elements :: ");
        for(int x : arr) {

            System.out.print(x + "  ");
        }

        System.out.println("\n");
    }
}