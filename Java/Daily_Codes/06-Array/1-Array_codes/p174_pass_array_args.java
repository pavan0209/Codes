/*
    Passing array as an argument to a method.
*/


class Solution {

    static void fun(int arr[]) {

        System.out.print("Array elements :: ");
        for(int x : arr) {
            System.out.print(x + "  ");
        }

        for(int i = 0; i < arr.length; i++) {

            arr[i] = arr[i] + 50;
        }
    }

    public static void main(String[] args) {

        int arr[] = {50, 100, 150};

        fun(arr);

        System.out.print("\nArray elements :: ");
        for(int x : arr) {

            System.out.print(x + "  ");
        }

        System.out.println("\n");
    }
}