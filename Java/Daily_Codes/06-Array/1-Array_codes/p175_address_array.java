/*
    Passing array as an argument to a method. and checking address of array.
*/


class Solution {

    static void fun(int arr[]) {

        System.out.println("\narr :: " + arr);
        arr[0] = 700;
    }

    public static void main(String[] args) {

        int arr[] = {50, 100, 150};

        System.out.println("\narr :: " + arr);

        fun(arr);

        System.out.println("\narr :: " + arr);

        System.out.println();
    }
}