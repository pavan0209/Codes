/*
 
*/

class Solution {
    public static void main(String[] args) {

        int arr[] = {10, 20, 30, 40};

        Solution s1 = new Solution();

        System.out.println();
        System.out.println(System.identityHashCode(arr[0]));
        System.out.println(System.identityHashCode(arr[1]));
        System.out.println(System.identityHashCode(arr[2]));
        System.out.println(System.identityHashCode(arr[3]) + "\n");

        s1.fun(arr);

        for(int x : arr) {

            System.out.print(x + "  ");
        }

        System.out.println("\n\n" + System.identityHashCode(arr[1]));
        System.out.println(System.identityHashCode(arr[2]));

        int x = 70; 
        int y = 80;

        System.out.println(System.identityHashCode(x));
        System.out.println(System.identityHashCode(y));

        System.out.println();
    }

    void fun(int arr[]) {

        arr[1] = 70;
        arr[2] = 80;
    }
}