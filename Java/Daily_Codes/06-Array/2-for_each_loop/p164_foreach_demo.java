/*
    for each loop in java.
*/

class Solution {
    public static void main(String[] args) {

        int arr[] = {10, 20, 30, 40, 50};

        System.out.print("\nPrinting array using for loop ::");
        for(int i = 0; i < arr.length; i++) {

            System.out.print("  " + arr[i]);
        }

        System.out.print("\nPrinting array using for each loop :: ");
        for(int x : arr) {

            System.out.print(x + "  ");
        }

        System.out.println();

        for(float x : arr) {

            System.out.print("  " + x);
        }

        System.out.println("\n");
    }
}
