/*
    Foreach loop.
*/

class Solution {
    public static void main(String[] args) {

        int arr[] = {10, 20, 30, 40, 50};

        for(float x : arr) {

            //System.out.println(arr[0]);
            System.out.println(x);
           // System.out.println(x[0]);   // error: array required, but float found
        }
    }
}
