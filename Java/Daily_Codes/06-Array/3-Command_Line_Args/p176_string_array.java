/*
    String array in JAVA.

*/

class Solution {
    public static void main(String[] args) {

        String friends[] = {"Ashish", "Badhe", "Kanha", "Rahul"};

        for(String x : friends) {

            System.out.print(x + "  ");
        }

        System.out.println();
    }
}