import java.util.Arrays;

/*
    Command Line Arguments in JAVA.
*/

class Solution {
    public static void main(String[] args) {

        String friends[] = {"Ashish", "Badhe", "Kanha", "Rahul"};

        System.out.println("\n" + Arrays.toString(friends));

        System.out.println("\nargs[0] :: " + args[0]);
        System.out.println("args[1] :: " + args[1]);
        System.out.println("args[2] :: " + args[2]);        

        System.out.println();
    }
}