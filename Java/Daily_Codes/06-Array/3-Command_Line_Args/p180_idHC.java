/*
 
*/

class Solution {
    public static void main(String[] args) {

        String arr[] = {"Ashish", "Ashish"};

        System.out.println(System.identityHashCode(arr[0]));
        System.out.println(System.identityHashCode(arr[1]));

        System.out.println("\n" + args[0]);
        System.out.println(args[1] + "\n");

        System.out.println(System.identityHashCode(args[0]));
        System.out.println(System.identityHashCode(args[1]));
        
        System.out.println();
    }
}