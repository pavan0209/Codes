/*
 *  Printing jagged array using for each loop
 */

class Solution {
    public static void main(String[] args) {

        int arr[][] = {{10,20,30},{40,50},{60}};

        for(int xarr[] : arr) {

            for(int x : xarr) {

                System.out.print(x + "  ");
            }
            System.out.println();
        }
    }   
}