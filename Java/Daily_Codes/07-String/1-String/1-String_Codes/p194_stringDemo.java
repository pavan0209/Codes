/*
    String in Java.
*/

class Solution {
    public static void main(String[] args) {

        String str1 = "Core2Web";
        String str2 = "Core2Web";
        char str3[] = {'C', '2', 'W'};

        System.out.println("\n" + str1);
        System.out.println(str2);
        System.out.println(str3);

        System.out.println();
    }
}