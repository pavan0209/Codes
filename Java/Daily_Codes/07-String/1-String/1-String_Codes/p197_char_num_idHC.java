/*
 
*/
class Solution {
    public static void main(String[] args) {

        char ch = 'A';
        int x = 65;

        System.out.println();
        System.out.println(System.identityHashCode(ch));
        System.out.println(System.identityHashCode(x));
        System.out.println();
    }
}