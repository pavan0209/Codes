/*
    String in JAVA.
*/

class Solution {
    public static void main(String[] args) {

        String str1 = "Kanha";
        String str2 = "Kanha";
        String str3 = new String("Kanha");
        String str4 = new String("Kanha");
        String str5 = new String("Rahul");
        String str6 = "Rahul";

        System.out.println(str1);
        System.out.println(str2);
        System.out.println(str3);
        System.out.println(str4);
        System.out.println(str5);
        System.out.println(str6);
    }
}