/*
    String Concat in JAVA.
*/

class Solution {
    public static void main(String[] args) {

        String str1 = "Pavan";
        String str2 = "Sonawane";

        System.out.println(str1);
        System.out.println(str2);

        str1.concat(str2);

        System.out.println(str1);
        System.out.println(str2);
    }
}