/*
 *  method : public String concat(String str);
 *  description : 
 *      -Concatenate string with another string.
 *  Parameters : String.
 *  Return type : String.
 */

import java.io.*;

class Solution {
    public static void main(String[] args) throws IOException {

        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

        System.out.print("\nEnter 1st String :: ");
        String str1 = br.readLine();

        System.out.print("Enter 2nd String :: ");
        String str2 = br.readLine();

        System.out.println("\nConcatenated String :: " + str1.concat(str2));
        System.out.println();
    }
}