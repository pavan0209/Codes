/* 
 *  concat() without library method.
 */

import java.io.*;

class Solution {

    String myStrConcat(String str1, String str2) {

        return str1+str2;
    }
    public static void main(String[] args) throws IOException {

        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        Solution s1 = new Solution();

        System.out.print("\nEnter 1st String :: ");
        String str1 = br.readLine();

        System.out.print("Enter 2nd String :: ");
        String str2 = br.readLine();

        System.out.println("\nConcatenated String :: " + s1.myStrConcat(str1,str2));
        System.out.println();
    }   
}