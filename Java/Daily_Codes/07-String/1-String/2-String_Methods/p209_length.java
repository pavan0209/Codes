/*
 *  method : public int length();
 *  description :
 *      - It returns the number of characters contained in Given string.
 *  Parameters : String
 *  return type : int
 */

import java.io.*;

class Solution {
    public static void main(String[] args) throws IOException { 

        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

        System.out.print("\nEnter String :: ");
        String str1 = br.readLine();

        System.out.println("length of '" + str1 + "' is :: " + str1.length());
        System.out.println();
    }
}