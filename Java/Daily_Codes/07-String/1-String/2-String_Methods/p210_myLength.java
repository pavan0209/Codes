/*
 *  String length() without length() method.
 */

 import java.io.*;

class Solution {

    int myStrLength(String str) {

        int count = 0;

       char arr[] = str.toCharArray();
        
        for(int i = 0; i < arr.length; i++) {
            
            count++;
        }

        //Another way:
        /*for(char ch : str.toCharArray()) {

            count++;
        }*/

        return count;
    }
    public static void main(String[] args) throws IOException { 

        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        Solution s1 = new Solution();
        

        System.out.print("\nEnter String :: ");
        String str1 = br.readLine();

        System.out.println("\nlength of '" + str1 + "' is :: " + s1.myStrLength(str1));
        System.out.println();
    }
}