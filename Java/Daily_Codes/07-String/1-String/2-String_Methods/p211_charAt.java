/*
 *  method : public char charAt(int index);
 *  description :
 *      - It returns the character located at specified index within the given string.
 *  parameters : int
 *  return type : char
 */

import java.io.*;

class Solution {
    public static void main(String[] args) throws IOException {

        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        
        System.out.print("\nEnter string :: ");
        String str = br.readLine();

        System.out.print("Enter index number :: ");
        int index = Integer.parseInt(br.readLine());

        System.out.println("\nCharacter at index " + index + " is :: " + str.charAt(index));
        System.out.println("Character at index 3 ::"+ str.charAt(3));
        //System.out.println(str.charAt(6));      //Exception in thread "main" java.lang.StringIndexOutOfBoundsException: String index out of range : 6
        System.out.println();
    }
}