/*
 *  charAt() without library method.
 */

import java.io.*;

class Solution {

    char myCharAt(String str, int index) {

        char ch[] = str.toCharArray();

        return ch[index];
    }
    public static void main(String[] args) throws IOException {

        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        Solution s1 = new Solution();
        
        System.out.print("\nEnter string :: ");
        String str = br.readLine();

        System.out.print("Enter index number :: ");
        int index = Integer.parseInt(br.readLine());

        
        System.out.println("\nCharacter at index " + index + " is :: " + s1.myCharAt(str, index));
        System.out.println();
    }
}