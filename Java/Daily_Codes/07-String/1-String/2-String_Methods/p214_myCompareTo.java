/*
 *  compareTo() without library method.
 */

import java.io.*;

class Solution {

    static int flag = 0;

    int myStrLength(String str) {

        int cnt = 0;

        for (char ch : str.toCharArray()) {

            cnt++;
        }
        return cnt;
    }

    char myCharAt(String str, int index) {

        char ch[] = str.toCharArray();

        return ch[index];
    }

    int myCompareTo(String str1, String str2) {

        int str1_len = myStrLength(str1);
        int str2_len = myStrLength(str2);

        if (str1_len != str2_len) {

            return str1_len - str2_len;
        } else {

            flag = 1;

            for (int i = 0; i < str1_len; i++) {

                char ch1 = myCharAt(str1, i);
                char ch2 = myCharAt(str2, i);

                if (ch1 != ch2) {

                    return ch1 - ch2;
                }
            }
            return 0;
        }
    }

    public static void main(String[] args) throws IOException {

        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        Solution s1 = new Solution();

        System.out.print("\nEnter 1st String :: ");
        String str1 = br.readLine();

        System.out.print("Enter 2nd String :: ");
        String str2 = br.readLine();

        int diff = s1.myCompareTo(str1, str2);

        if (flag == 0) {

            System.out.println("\nStrings differ in length by :: " + diff);
        } else {

            if (diff == 0) {

                System.out.println("\nStrings are equal that means differece is :: " + diff);
            } else {
                System.out.println("\nStrings are not equal and difference is :: " + diff);
            }
        }
        System.out.println();
    }
}