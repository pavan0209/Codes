/*
 *  method : public int compareToIgnoreCase(String str2);
 *  description :
 *      -This method compares 2 strings(case insensitive), if both strings are equal, 
 *       then it returns 0 otherwise returns the difference.
 *  parameters : String (another string).
 *  return type : int. 
 */

import java.io.*;

class Solution {
    public static void main(String[] args) throws IOException {

        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

        System.out.print("\nEnter 1st String :: ");
        String str1 = br.readLine();

        System.out.print("Enter 2nd String :: ");
        String str2 = br.readLine();

        // System.out.println(str1.compareToIgnoreCase(str2));

        int ret = str1.compareToIgnoreCase(str2);

        if (ret != 0) {

            System.out.println("\nStrings are not equal and difference is :: " + ret);
        } else {

            System.out.println("\nStrings are equal means there difference is :: " + ret);
        }

        System.out.println();
    }
}