/*
 *  method : public boolean equals(Object anObject);
 *  description :
 *      - This method checks content of two strings and return true if both string are equal (case sensitive)
 *        else it returns false.
 *  parameters : Object (anObject);
 *  return type : boolean. 
 */

import java.io.*;

class Solution {
    public static void main(String[] args) throws IOException {

        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

        System.out.print("\nEnter 1st String :: ");
        String str1 = br.readLine();

        System.out.print("Enter 2nd String :: ");
        String str2 = br.readLine();

        // System.out.println(str1.equals(str2));

        boolean val = str1.equals(str2);

        if (val == true) {

            System.out.println("\nStrings are equal");
        } else {

            System.out.println("\nStrings are not equal");
        }

        System.out.println();
    }
}