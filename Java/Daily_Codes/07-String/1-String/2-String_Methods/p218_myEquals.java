/*
 *  equals() without library method.
 */

import java.io.*;

class Solution {

    int myStrLength(String str) {

        int count = 0;

        for (char ch : str.toCharArray()) {

            count++;
        }
        return count;
    }

    char myCharAt(String str, int index) {

        char ch[] = str.toCharArray();

        return ch[index];
    }

    boolean myEquals(String str1, String str2) {

        int len = myStrLength(str1);

        for (int i = 0; i < len; i++) {

            char ch1 = myCharAt(str1, i);
            char ch2 = myCharAt(str2, i);

            if (ch1 != ch2) {

                return false;
            }
        }
        return true;
    }

    public static void main(String[] args) throws IOException {

        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        Solution s1 = new Solution();

        System.out.print("\nEnter 1st String :: ");
        String str1 = br.readLine();

        System.out.print("Enter 2nd String :: ");
        String str2 = br.readLine();

        boolean retVal = s1.myEquals(str1, str2);

        if (retVal == true) {

            System.out.println("\nStrings are equal");
        } else {

            System.out.println("\nStrings are not equal");
        }
        System.out.println();
    }
}