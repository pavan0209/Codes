/*
 *  method : public char[] toCharArray(String Str);
 *  description :
 *      - This method converts String to character array.
 *  parameters : String.
 *  return type : char[] (Character array). 
 */

import java.io.*;

class Solution {
    public static void main(String[] args) throws IOException {

        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

        System.out.print("\nEnter String :: ");
        String str = br.readLine();

        System.out.print("\nCharacter array :: ");

        char chArr[] = str.toCharArray();

        for (char ch : chArr) {

            System.out.print(ch + "  ");
        }

        System.out.println("\n");
    }
}