/*
 *  toCharArray() without library method.  
 */

import java.io.*;

class Solution {

    char[] myToCharArray(String str) {

        char ch[] = new char[str.length()];
        for (int i = 0; i < str.length(); i++) {

            ch[i] = str.charAt(i);
        }
        return ch;
    }

    public static void main(String[] args) throws IOException {

        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        Solution s1 = new Solution();

        System.out.print("\nEnter String :: ");
        String str = br.readLine();

        char carr[] = s1.myToCharArray(str);

        System.out.print("\ncharacter array :: ");

        for (char ch : carr) {

            System.out.print(ch + "  ");
        }

        System.out.println("\n");
    }
}