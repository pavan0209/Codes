/*
    Write a program to check whether the length of strings is equal or not.
*/

import java.util.*;

class Solution {

    static int myStrLen(String str) {

        int count = 0;

        for(char ch : str.toCharArray()) {

            count++;
        }

        return count;
    }

    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);

        System.out.println();

        System.out.print("Enter 1st String :: ");
        String str1 = sc.next();

        System.out.print("Enter 2nd String :: ");
        String str2 = sc.next();

        if(myStrLen(str1) == myStrLen(str2)) {

            System.out.println("\nStrings are equal in length\n");
        }
        else {

            System.out.println("\nStrings are not equal in length\n");
        }

        sc.close();
    }
}