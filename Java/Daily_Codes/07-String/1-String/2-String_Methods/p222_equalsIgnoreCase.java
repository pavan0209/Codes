/*
 *  method : public boolean equalsIgnoreCase(Object anObject);
 *  description :
 *      - This method checks content of two strings and return true if both string are equal (case insensitive)
 *        else it returns false.
 *  parameters : String (str2).
 *  return type : boolean. 
 */

import java.io.*;

class Solution {
    public static void main(String[] args) throws IOException {

        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

        System.out.print("\nEnter 1st string :: ");
        String str1 = br.readLine();

        System.out.print("Enter 2nd string :: ");
        String str2 = br.readLine();

        if (str1.equalsIgnoreCase(str2)) {

            System.out.println("\nStrings are equal\n");
        } else {

            System.out.println("\nStrings are not equal\n");
        }

        br.close();
    }
}