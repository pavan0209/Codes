/*
 *  equalsIgnoreCase() without library function.
 */

import java.io.*;

class Solution {

    int myStrLen(String str) {

        int count = 0;

        for (char ch : str.toCharArray()) {
            count++;
        }
        return count;
    }

    char myCharAt(String str, int index) {

        char ch[] = str.toCharArray();
        return ch[index];
    }

    boolean myEqualsIgnoreCase(String str1, String str2) {

        int len = myStrLen(str1);

        for (int i = 0; i < len; i++) {

            char ch1 = myCharAt(str1, i);
            char ch2 = myCharAt(str2, i);

            if (ch1 != ch2) {

                if (ch1 - ch2 == 32 || ch1 - ch2 == -32) {

                    continue;
                } else {

                    return false;
                }
            }
        }
        return true;
    }

    public static void main(String[] args) throws IOException {

        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        Solution s1 = new Solution();

        System.out.print("\nEnter 1st String :: ");
        String str1 = br.readLine();

        System.out.print("Enter 2nd String :: ");
        String str2 = br.readLine();

        boolean ret = s1.myEqualsIgnoreCase(str1, str2);

        if (ret == true) {

            System.out.println("\nStrings are equal\n");
        } else {

            System.out.println("\nStrings are not equal\n");
        }

        br.close();
    }
}