/*
 *  method : public int indexOf(int ch, int fromIndex);
 *  description :
 *      - Finds the first instance of the character in the given string.
 *  parameters : character(ch to find), Integer (start index).
 *  return type : Integer.
 */

import java.io.*;

class Solution {
    public static void main(String[] args) throws IOException {

        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

        System.out.print("\nEnter String :: ");
        String str = br.readLine();

        System.out.print("Enter character to be searched in String :: ");
        char ch = (char) br.read();

        // br.read();
        br.skip(1);

        System.out.print("Enter the starting position where you want to search :: ");
        int st_index = Integer.parseInt(br.readLine());

        System.out.println("\nfirst instance of " + ch + " is at index :: " + str.indexOf(ch, st_index));
        System.out.println();

        br.close();
    }
}