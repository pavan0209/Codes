/*
 *  indexOf() method without using library method.
 */

import java.io.*;

class Solution {

    int myIndexOf(String str, char ch, int st_index) {

        char carr[] = str.toCharArray();

        for (int i = 0; i < carr.length; i++) {
            if (carr[i] == ch) {

                return i;
            }
        }

        return -1;
    }

    public static void main(String[] args) throws IOException {

        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        Solution s1 = new Solution();

        System.out.print("\nEnter String :: ");
        String str = br.readLine();

        System.out.print("Enter character to be searched in String :: ");
        char ch = (char) br.read();

        br.read();

        System.out.print("Enter the starting position where you want to search :: ");
        int st_index = Integer.parseInt(br.readLine());
        // int st_index = (char)(br.read());

        System.out.println("\nfirst instance of " + ch + " is at index :: " + s1.myIndexOf(str, ch, st_index));
        System.out.println();
    }
}