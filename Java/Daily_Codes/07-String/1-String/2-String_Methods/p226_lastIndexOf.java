/*
 *  method : public int lastIndex(int ch, int fromIndex);
 *  description : 
 *      - finds the last instance of the character in the given String.
 *  parameters : character (ch to find), Interger (index to search upto).
 *  return type : Integer. 
 */

import java.io.*;

class Solution {
    public static void main(String[] args) throws IOException {

        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

        System.out.print("\nEnter String :: ");
        String str = br.readLine();

        System.out.print("Enter character to be searched in String :: ");
        char ch = (char) br.read();

        br.skip(1);

        System.out.print("Enter index to search up to that index :: ");
        int index = Integer.parseInt(br.readLine());

        System.out.println("\nLast instance of " + ch + " is at index :: " + str.lastIndexOf(ch, index));

        System.out.println();

        br.close();
    }
}