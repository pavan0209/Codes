/*
 *  lastIndexOf() method without using library method.
 */

import java.io.*;

class Solution {

    int myLastIndexOf(String str, char ch, int to_index) {

        char carr[] = str.toCharArray();
        int index = -1;

        for (int i = 0; i < carr.length; i++) {
            if (carr[i] == ch) {

                index = i;
            }
        }

        return index;
    }

    public static void main(String[] args) throws IOException {

        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        Solution s1 = new Solution();

        System.out.print("\nEnter String :: ");
        String str = br.readLine();

        System.out.print("Enter character to be searched in String :: ");
        char ch = (char) br.read();

        br.read();

        System.out.print("Enter the starting position where you want to search :: ");
        int to_index = Integer.parseInt(br.readLine());
        // int to_index = (char)(br.read());

        System.out.println("\nfirst instance of " + ch + " is at index :: " + s1.myLastIndexOf(str, ch, to_index));
        System.out.println();
    }
}