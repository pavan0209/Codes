/*
 *  method :  public String replace(char oldChar, char newChar);
 *  description :
 *      - replaces every instances of a character in the given string with a new character.
 *  parameters : character (old character), character (new, character).
 *  return type : String.
 */

import java.io.*;

class Solution {
    public static void main(String[] args) throws IOException {

        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

        System.out.print("\nEnter String :: ");
        String str = br.readLine();

        System.out.print("Enter character to be replaced in String :: ");
        char oldChar = (char) br.read();

        br.read();

        System.out.print("Enter character to be displayed at position of old character :: ");
        char newChar = (char) br.read();

        System.out.println("\nString before characters replaced:: " + str);
        System.out.println("String after characters replaced:: " + str.replace(oldChar, newChar));

        System.out.println();

        br.close();
    }
}