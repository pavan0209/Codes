/*
 *  replace() without library method.
 */

import java.io.*;

class Solution {

    String myReplace(String str, char oldChar, char newChar) {

        char carr[] = str.toCharArray();
        String newStr = "";
        int flag = 0;

        for(int i = 0; i < carr.length; i++) {

            if(carr[i] == oldChar) {
                flag  = 1;
                carr[i] = newChar;
            }
            newStr = newStr + carr[i];
        }
        if(flag == 1) {
            return newStr;
        }
        else {
            return Integer.toString(-1);
        }

    }

    public static void main(String[] args) throws IOException {

        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        Solution s1 = new Solution();
        
        System.out.print("\nEnter String :: ");
        String str = br.readLine();

        System.out.print("Enter character to be replaced in String :: ");
        char oldChar = (char)br.read();

        br.read();

        System.out.print("Enter character to be displayed at position of old character :: ");
        char newChar = (char)br.read();

        System.out.println("\nString before characters replaced:: " + str);

        String newStr = s1.myReplace(str, oldChar, newChar);

        if(newStr.equals("-1")) {
            System.out.println("Character to be replaced not found");
        }
        else {
            System.out.println("String after characters replaced :: " + newStr);
        }

        System.out.println();

        br.close();
    }
}