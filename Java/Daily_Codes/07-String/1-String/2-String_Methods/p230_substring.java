/*
 *  method : public String substring(int index);
 *  description :
 *      - creates a substring of the given string starting at a specified index and ending at
 *        the end of given string.
 *  parameters : Integer(index of the string).
 *  return type : String.
 */

import java.io.*;

class Solution {
    public static void main(String[] args) throws IOException {

        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

        System.out.print("\nEnter String :: ");
        String str = br.readLine();

        System.out.print("Enter Index from where you want substring:: ");
        int st_index = Integer.parseInt(br.readLine());

        System.out.println("\nSubstring :: " + str.substring(st_index));
        System.out.println("\nSubstring :: " + str.substring(st_index , 5));

        System.out.println();

    }
}