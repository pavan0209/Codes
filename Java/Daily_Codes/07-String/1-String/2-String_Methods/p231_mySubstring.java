/*
 *  substring() without library method. 
 */

import java.io.*;

class Solution {

    String mySubstring(String str, int st_index, int end_index) {

        char[] ch = str.toCharArray();
        String newStr = "";

        for (int i = st_index; i <= end_index; i++) {

            newStr = newStr + ch[i];
        }

        return newStr;

    }

    public static void main(String[] args) throws IOException {

        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        Solution s1 = new Solution();

        System.out.print("\nEnter String :: ");
        String str = br.readLine();

        System.out.print("Enter Index from where you want substring:: ");
        int st_index = Integer.parseInt(br.readLine());

        System.out.print("Enter Index till you want substring:: ");
        int end_index = Integer.parseInt(br.readLine());

        System.out.println("\nSubstring :: " + s1.mySubstring(str, st_index, end_index));

        System.out.println();
    }
}