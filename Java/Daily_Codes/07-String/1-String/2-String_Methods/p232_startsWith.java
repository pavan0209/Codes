/*
 *  method : public boolean startsWith(String prefix);
 *  description :
 *      - Tests if this string starts with the specified prefix.
 *  parameters : String (prefix)
 *  return type : boolean
 */

import java.io.*;

class Solution {
    public static void main(String[] args) throws IOException {

        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

        System.out.print("\nEnter String :: ");
        String str = br.readLine();

        System.out.print("Enter string which you want to find :: ");
        String seacrchStr = br.readLine();

        System.out.print("From where you want to start comparison:: ");
        int st_index = Integer.parseInt(br.readLine());

        System.out.println("is '" + seacrchStr + "' starts from index " + st_index + " :: "
                + str.startsWith(seacrchStr, st_index));
    }
}