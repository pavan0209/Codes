/*
 *  startWith() without using library method.
 */

import java.io.*;

class Solution {

    boolean myStartsWith(String str, String searchStr, int offset) {

        if (offset > str.length() || offset < 0) {

            return false;
        } else if (str.length() < searchStr.length()) {

            return false;
        } else {

            char ch[] = str.toCharArray();
            int temp = 0;

            for (int i = offset; i < searchStr.length(); i++) {

                if (ch[i] != searchStr.charAt(temp++)) {

                    return false;
                }
            }
            return true;
        }
    }

    public static void main(String[] args) throws IOException {

        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        Solution s1 = new Solution();

        System.out.print("\nEnter String :: ");
        String str = br.readLine();

        System.out.print("Enter string which you want to find :: ");
        String seacrchStr = br.readLine();

        System.out.print("From where you want to start comparison:: ");
        int st_index = Integer.parseInt(br.readLine());

        System.out.println("is " + seacrchStr + " starts from index " + st_index + " :: "
                + s1.myStartsWith(str, seacrchStr, st_index));
    }
}