/*
 *  method: public boolean endsWith(String suffix);
 *  description :
 *      - Predicate which determines if the string ends with a given suffix.
 *      - If the suffix is an empty string, true is returned.
 *      - throws NullPointerException if suffix is null.
 *  parameters : prefix String to compare.
 *  return type : boolean.
 */

import java.io.*;

class Solution {
    public static void main(String[] args) throws IOException {

        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

        System.out.print("\nEnter String :: ");
        String str = br.readLine();

        System.out.print("Enter string which you want to find endsWith ?? :: ");
        String searchStr = br.readLine();

        System.out.println("\nis '" + str + "' ends with '" + searchStr + "' :: "
        + str.endsWith(searchStr));
        
        System.out.println();
    }

}