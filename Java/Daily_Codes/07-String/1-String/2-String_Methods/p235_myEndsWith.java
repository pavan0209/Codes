/*
 *  endsWith() without using library method.
 */

import java.io.*;

class Solution {

    boolean myEndsWith(String str, String searchStr) {

        char ch[] = str.toCharArray();

        if (ch.length < searchStr.length()) {

            return false;

        } else {

            int temp = 0;
            int st_index = str.length() - searchStr.length();
            for (int i = st_index; i < ch.length; i++) {

                if (ch[i] != searchStr.charAt(temp++)) {

                    return false;
                }
            }
            return true;
        }
    }

    public static void main(String[] args) throws IOException {

        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        Solution s1 = new Solution();

        System.out.print("\nEnter String :: ");
        String str = br.readLine();

        System.out.print("Enter string which you want to find endsWith?? :: ");
        String searchStr = br.readLine();

        System.out.println("\nis '" + str + "' ends with '" + searchStr + "' :: " + s1.myEndsWith(str, searchStr));

        System.out.println();
    }
}