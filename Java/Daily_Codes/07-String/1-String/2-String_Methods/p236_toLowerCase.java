/*
 *  method : public String toLowerCase();
 *  description :
 *      - it lowercases all characters in the given string.
 *  parameters : no parameter.
 *  return type : String.
 */

import java.io.*;

class Solution {
    public static void main(String[] args) throws IOException {

        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

        System.out.print("\nEnter String :: ");
        String str = br.readLine();
        
        System.out.println("\nString '" + str + "' in lowecase is :: " + str.toLowerCase());

        System.out.println();
    }
}