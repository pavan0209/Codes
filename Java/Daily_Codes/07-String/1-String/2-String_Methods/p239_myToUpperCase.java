/*
 *  toUpperCase() without library function.
 */

import java.io.*;

class Solution {

    String myToUpperCase(String str) {

        char[] ch = str.toCharArray();
        String newStr = "";

        for (int i = 0; i < ch.length; i++) {

            char temp = ch[i];

            if (ch[i] >= 97 && ch[i] <= 122) {

                temp = (char) (ch[i] - 32);
            }
            newStr = newStr + temp;
        }

        return newStr;
    }

    public static void main(String[] args) throws IOException {

        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        Solution s1 = new Solution();

        System.out.print("\nEnter String :: ");
        String str = br.readLine();

        System.out.println("\nString '" + str + "' in UPPERCASE is :: " + s1.myToUpperCase(str));

        System.out.println();
    }
}