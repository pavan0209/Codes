/*
 *  method : public String trim();
 *  description :
 *      - trims all the white spaces before and after of the string.
 *  parameters : no parameter.
 *  return type : String.
 */

import java.io.*;

class Solution {
    public static void main(String[] args) throws IOException {

        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

        System.out.print("\nEnter String :: ");
        String str = br.readLine();

        System.out.println("\nString before trim :: " + str);
        System.out.println("String after trim :: " + str.trim());

        System.out.println();
    }
}