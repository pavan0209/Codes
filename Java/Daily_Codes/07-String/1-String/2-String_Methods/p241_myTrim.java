/*
 *  trim() without library function.
 */

import java.io.*;

class Solution {

    String myTrim(String str) {

        char ch[] = str.toCharArray();
        String newStr = "";

        int len = ch.length - 1;
        int i = 0;

        while (true) {

            if (ch[i] == 32)
                i++;
            else
                break;
        }

        while (true) {

            if (ch[len] == 32)
                len--;
            else
                break;
        }

        for (int j = i; j <= len; j++) {
            newStr = newStr + ch[j];
        }
        return newStr;
    }

    public static void main(String[] args) throws IOException {

        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        Solution s1 = new Solution();

        System.out.print("\nEnter String :: ");
        String str = br.readLine();

        System.out.println("\nString before trim :: " + str + ".");
        System.out.println("String after trim :: " + s1.myTrim(str) + ".");

        System.out.println();
    }
}