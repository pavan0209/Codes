/*
 *  method : public String[] split(String delimiter);
 *  description :
 *      - Splits the string around matches or regular expressions.
 *  parameters : delimiter to match.
 *  return type : String[] (array of split Strings).
 */

 import java.io.*;

 class Solution {
     public static void main(String[] args) throws IOException {
 
         BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
 
         System.out.print("\nEnter String :: ");
         String str = br.readLine();
 
         System.out.println("\nString before split :: " + str);

         String strRes[] = str.split("na");

         for(String resStr : strRes) {

            System.out.println(resStr);
         }

         System.out.println();
     }
 }