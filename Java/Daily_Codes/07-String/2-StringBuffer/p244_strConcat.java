/*
    string concat.
*/

class Solution {
    public static void main(String[] args) {

        String str1 = "Pavan";
        String str2 = str1.concat("Sonawane");

        System.out.println(str1);
        System.out.println(str2);
    }
}