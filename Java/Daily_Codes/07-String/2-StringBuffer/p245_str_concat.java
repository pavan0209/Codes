/*
    string concat.
*/

class Solution {
    public static void main(String[] args) {

        String str1 = "Pavan";      //create object in String Constant Pool.

        System.out.println("str1 :: " + str1);
        System.out.println("idHC of str1 :: " + System.identityHashCode(str1));
        
        str1 = str1.concat("Sonawane");     //internally calls new and creates object on heap.

        System.out.println("str1 :: " + str1);
        System.out.println("idHC of str1 :: " + System.identityHashCode(str1));
        
        System.out.println();
    }
}