/*
    StringBuffer class in JAVA. Strings created using StringBuffer class are mutable we can change content
    old string. While Strings created using String class are immutable.
*/

class Solution {
    public static void main(String[] args) {

        //StringBuffer sb = "Pavan";      //error: incompatible types: String cannot be converted to StringBuffer

        StringBuffer str1 = new StringBuffer("Pavan");

        System.out.println("str1 :: " + str1);
        System.out.println("idHC of str1 :: " + System.identityHashCode(str1));
        
        str1.append("Sonawane");     

        System.out.println("str1 :: " + str1);
        System.out.println("idHC of str1 :: " + System.identityHashCode(str1));
        
        }
}