/*
    StringBuffer capacity().
*/
class Solution {
    public static void main(String[] args) {

        StringBuffer str1 = new StringBuffer("Pavan");
        System.out.println(System.identityHashCode(str1));
        System.out.println("\ncapacity :: " + str1.capacity());

        System.out.println(System.identityHashCode(str1));

        StringBuffer str2 = new StringBuffer();

        System.out.println("default capacity :: " + str2.capacity());
    }
}