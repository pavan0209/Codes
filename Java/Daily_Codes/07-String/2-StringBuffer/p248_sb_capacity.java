/*
 *  StringBuffer capacity().
 */

class Solution {
    public static void main(String[] args) {

        StringBuffer sb = new StringBuffer();

        System.out.println("\ncapcity :: " + sb.capacity());
        System.out.println("Content :: " + sb);

        sb.append("Pavan");

        System.out.println("\ncapcity :: " + sb.capacity());
        System.out.println("Content :: " + sb);

        sb.append("Arun");

        System.out.println("\ncapcity :: " + sb.capacity());
        System.out.println("Content :: " + sb);

        sb.append("Sonawane");

        System.out.println("\ncapacity :: " + sb.capacity());
        System.out.println("Content :: " + sb);

        System.out.println();
    }
}