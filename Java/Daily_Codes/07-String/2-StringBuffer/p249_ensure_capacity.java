/*
 *  ensure capacity in JAVA.
 */

class Solution {
    public static void main(String[] args) {

        StringBuffer sb = new StringBuffer(100);

        sb.append("Pavan");
        sb.append("Arun");

        System.out.println("\nString :: " + sb);
        System.out.println("capacity :: " + sb.capacity());

        sb.append("Sonawane");

        System.out.println("\nString :: " + sb);
        System.out.println("capacity :: " + sb.capacity());

        System.out.println();

    }
}