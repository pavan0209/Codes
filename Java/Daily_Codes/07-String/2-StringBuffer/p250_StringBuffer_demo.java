/*
 
*/

class Solution {
    public static void main(String[] args) {

        String str1 = "Pavan";
        String str2 = new String("Sonawane");

        StringBuffer str3 = new StringBuffer("Engineer");

        /*String str4 = str3.append(str1);    // error: incompatible types: StringBuffer cannot be converted to String
        String str5 = str1.append(str3);    // error: cannot find symbol
        */
        StringBuffer str6 = str3.append(str1);

        System.out.println("\nstr1 :: " + str1);
        System.out.println("str2 :: " + str2);
        System.out.println("str3 :: " + str3);
        /*System.out.println("str4 :: " + str4);
        System.out.println("str5 :: " + str5);*/
        System.out.println("str6 :: " + str6);

        System.out.println();
    }
}