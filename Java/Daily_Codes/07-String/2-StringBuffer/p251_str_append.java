/*
 *  append() in StringBuffer. 
 */
class Solution {
    public static void main(String[] args) {

        String str1 = "Pavan";
        String str2 = new String("Sonawane");

        StringBuffer str3 = new StringBuffer("Engineer");

        //String str4 = str1.concat(str3);    // error: incompatible types: StringBuffer cannot be converted to String

        StringBuffer str5 = str3.append(str1);

        System.out.println("\nstr1 :: " + str1);
        System.out.println("str2 :: " + str2);
        System.out.println("str3 :: " + str3);
        System.out.println("str5 :: " + str5);
        
        System.out.println();


    }
}