/*
 
*/

class Solution {
    public static void main(String[] args) {

        String str1 = "Pavan";
        String str2 = new String("Sonawane");

        StringBuffer str3 = new StringBuffer("Engineer");

        str1.concat(str2);
        str3.append(str2);

        System.out.println("\nstr1 :: " + str1);
        System.out.println("str2 :: " + str2);
        System.out.println("str3 :: " + str3);
    }
}