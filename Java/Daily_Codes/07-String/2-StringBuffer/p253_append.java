/*
 *  method : public synchronized StringBuffer append(String str);
 *  description :
 *      - appends one string to another string.
 *      - If string is null, the string "null" is appended.
 *  parameter : String, StringBuffer.
 *  return type : StringBuffer.
 */

import java.io.*;

class Solution {
    public static void main(String[] args) throws IOException {

        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));   

        System.out.print("\nEnter String :: ");
        String str1 = br.readLine();

        StringBuffer sb = new StringBuffer(str1);

        System.out.print("Enter string which you want to append :: ");
        String str2 = br.readLine();
        
        System.out.println("\nappended String :: " + sb.append(str2));

        System.out.println();
        br.close();
    }
}