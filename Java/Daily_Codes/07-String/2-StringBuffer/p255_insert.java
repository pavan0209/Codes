/*
 *  method : public synchronized StringBuffer insert(int offset, String str);
 *  description :
 *      - inserts one string to another string.
 *  parameter : integer, (StringBuffer, String).
 *  return type : StringBuffer.
 */

import java.io.*;

class Solution {
    public static void main(String[] args) throws IOException {

        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

        System.out.print("\nEnter String :: ");
        String str1 = br.readLine();

        StringBuffer sb = new StringBuffer(str1);

        System.out.print("Enter string which you want to insert :: ");
        String str2 = br.readLine();

        System.out.print("Enter position where you want to insert :: ");
        int index = Integer.parseInt(br.readLine());

        System.out.println("\nString after inserting string/characters:: " + sb.insert(index, str2));

        System.out.println();
        br.close();
    }
}