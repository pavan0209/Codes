/*
 *  method : public synchronized StringBuffer delete(int start, int end);
 *  description :
 *      - delete characters from this start index to end index in given string.
 *  parameter : integer(starting position), integer(ending position).
 *  return type : StringBuffer.
 */

import java.io.*;

class Solution {
    public static void main(String[] args) throws IOException {

        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

        System.out.print("\nEnter String :: ");
        String str1 = br.readLine();

        StringBuffer sb = new StringBuffer(str1);

        System.out.print("\nEnter starting position from where you want to delete :: ");
        int st_index = Integer.parseInt(br.readLine());

        System.out.print("Enter last position till you want to delete :: ");
        int end_index = Integer.parseInt(br.readLine());

        System.out.println("\nString after deleting :: " + sb.delete(st_index, end_index));

        System.out.println();
        br.close();
    }
}