/*
 *  method : public synchronized StringBuffer reverse();
 *  description :
 *      - Reverse the characters in this StringBuffer.
 *  parameter : No Parameter.
 *  return type : StringBuffer.
 */

import java.io.*;

class Solution {
    public static void main(String[] args) throws IOException {

        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

        System.out.print("\nEnter String :: ");
        String str1 = br.readLine();
        String str2 = str1;

        StringBuffer sb = new StringBuffer(str1);
        StringBuffer sb2 = new StringBuffer(str2);

        System.out.println("\nreversed String :: " + sb.reverse());

        str2 = sb2.reverse().toString();

        System.out.println("\nreversed String :: " + str2);

        System.out.println();
        br.close();
    }
}