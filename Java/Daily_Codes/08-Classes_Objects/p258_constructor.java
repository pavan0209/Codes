/*
 *  - to intitialize instance variable constructors are used.
 *  - Constructor does not have return type
 *  - class name and constructor name is same.
 */
class Solution {

    int y = 20; // Stored in constructor.

    static void fun() {

        int z = 30;
        System.out.println("\nz :: " + z);
        System.out.println("idHC of Z :: " + System.identityHashCode(z));
    }

    public static void main(String[] args) {

        int x = 10;

        fun();
        System.out.println("\nx :: " + x);
        System.out.println("idHC of x :: " + System.identityHashCode(x));

        System.out.println();
    }
}