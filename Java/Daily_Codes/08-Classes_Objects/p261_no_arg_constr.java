/*
 *  
 */

 class Solution {

    Solution() {

        System.out.println("In Solution Constructor");
    }

    int x = 10;

    public static void main(String[] args) {

        System.out.println("In main");

        Solution obj = new Solution();
        
        System.out.println("x :: " + obj.x);
        System.out.println("y :: " + obj.y);

    }

    int y = 20;
}