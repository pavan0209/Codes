/* 
*/

class Solution {

    Solution() {
        System.out.println("In constructor");
    }

    void fun() {

        Solution s1 = new Solution();   
    }

    public static void main(String[] args) {

        Solution s1 = new Solution();
        Solution s2 = new Solution();
        
        s1.fun();
    }
}