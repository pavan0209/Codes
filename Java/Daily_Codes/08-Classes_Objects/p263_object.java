/*
 *
 */

class Solution {

    int x = 10;
    int y = 20;

    void display() {

        System.out.println("x :: " + x);
        System.out.println("y :: " + y);
    }

    public static void main(String[] args) {

        Solution obj = new Solution();
        
        System.out.println("x :: " + obj.x);
        System.out.println("y :: " + obj.y);
    }
}