/*
 *  Real-time example on class and object  
 */

class Project {

    String ProjName = "OnlineEdu";
    int noOfEmp = 20;

    void clientInfo() {

        String clientName = "Core2Web";

        System.out.println("\nClient Name :: " + clientName);
        System.out.println("Project Name :: " + ProjName);
        System.out.println("no of Employee :: " + noOfEmp);
    }

    public static void main(String[] args) {

        Project prj = new Project();
        prj.clientInfo();

        System.out.println();
    }
}