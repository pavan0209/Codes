/*
 *  Real-Life example of instance method(non-static) and object. (MacD)
*/

class MacD {

    int noOfItems = 5;
    String product = "Fries";

    void display() {

        System.out.println("\nNo. Of Items :: " + noOfItems);
        System.out.println("Product :: " + product);
    }

    public static void main(String[] args) {

        MacD m1 = new MacD();
        m1.display();

        System.out.println();
    }
}