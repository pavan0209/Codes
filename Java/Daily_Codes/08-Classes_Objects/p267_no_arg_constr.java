/* 
*/

class Solution {

    int noCourses = 8;
    String favCourse = "Bootcamp";

    void displayInfo() {

        System.out.println("\nNo. of courses :: " + noCourses);
        System.out.println("Favorite Course :: " + favCourse);
    }

    public static void main(String[] args) {

        Solution s1 = new Solution();

        s1.displayInfo();

        System.out.println("\nNo of Courses :: " + s1.noCourses);
        System.out.println("Favorite Course :: " + s1.favCourse);

        System.out.println();
    }
}