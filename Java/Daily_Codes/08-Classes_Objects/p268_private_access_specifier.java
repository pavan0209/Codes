/*
 *  Private access specifier has a scope which is only within class. 
 */

class Solution {

    int noCourses = 8;
    private String favCourse = "Bootcamp";

    void displayInfo() {

        System.out.println("\nNo. of courses :: " + noCourses);
        System.out.println("Favorite Course :: " + favCourse);
    }
}

class Client{
    public static void main(String[] args) {

        Solution s1 = new Solution();

        s1.displayInfo();

        System.out.println("\nNo of Courses :: " + s1.noCourses);
        //System.out.println("Favorite Course :: " + s1.favCourse);   //error: favCourse has private access in Solution

        System.out.println();
    }
}