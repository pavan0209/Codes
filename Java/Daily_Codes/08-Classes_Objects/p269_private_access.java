/*
 
*/

class Solution {

    int x = 10;
    private int y = 20;

    void fun() {

        System.out.println("\nx :: " + x);
        System.out.println("y :: " + y);
    }
}

class Client {
    public static void main(String[] args) {

        Solution obj = new Solution();
        obj.fun();

        System.out.println("x :: " + obj.x);
        System.out.println("y :: " + obj.y);    //error: y has private access in Solution

        System.out.println("x :: " + x);        //error: cannot find symbol
        System.out.println("y :: " + y);        //error: cannot find symbol
    }
}