/*
 *  Real-Life Example on class and object
 */

class Employee {

    int empId = 10;
    String empName = "Pavan";

    void empInfo() {

        System.out.println("\nId :: " + empId);
        System.out.println("name :: " + empName);
    }
}

class Solution {
    public static void main(String[] args) {

        Employee emp = new Employee();
        
        emp.empInfo();

        System.out.println("\nId :: " + emp.empId);
        System.out.println("name :: " + emp.empName);

        System.out.println();
    }
}