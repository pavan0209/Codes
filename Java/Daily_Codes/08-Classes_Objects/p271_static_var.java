/*
 *  Static variable and Instance variable
 *  -Static variable is also called as class variable
 *  -Static variable is stored in Static block.
 *  -Static variable is common for all objects.
 */

class Employee {

    int empId = 10;
    String empName = "Pavan";
    static int y = 50;

    void empInfo () {

        System.out.println("\nId :: " + empId);
        System.out.println("name :: " + empName);
        System.out.println("y :: " + y);
    }
}

class Solution {
    public static void main(String[] args) {

        Employee emp1 = new Employee();
        Employee emp2 = new Employee();
        
        emp1.empInfo();
        emp2.empInfo();

        System.out.println("------------------");

        emp2.empId = 20;
        emp2.empName = "Vinit";
        emp2.y = 5000;

        emp1.empInfo();
        emp2.empInfo();

        System.out.println();
    }
}