/*
 *  Real-life example of classes and Objects (WebSeries) 
 */

class WebSeries {

    String seriesName = "Flames";
    static int totalSeasons = 2;
    int totalEpisodes = 15;

    void seriesInfo () {

        System.out.println("\nSeries Name :: " + seriesName);
        System.out.println("Total Seasons :: " + totalSeasons);
        System.out.println("Total Episodes :: " + totalEpisodes);
    }
}

class Solution {
    public static void main(String[] args) {

        WebSeries ws1 = new WebSeries();
        WebSeries ws2 = new WebSeries();

        ws1.seriesInfo();
        ws2.seriesInfo();

        System.out.println("\n-----------------");

        ws2.seriesName = "Stranger Things";
        ws2.totalSeasons = 4;
        ws2.totalEpisodes = 35;

        ws1.seriesInfo();
        ws2.seriesInfo();

        System.out.println();
    }
}