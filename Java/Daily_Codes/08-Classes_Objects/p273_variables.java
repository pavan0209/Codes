/*
 *    
 */

class Demo {

    int x = 10;
    private int y = 20;
    static int z = 30;

    void display() {

        System.out.println("\nx :: " + x);
        System.out.println("y :: " + y);
        System.out.println("z :: " + z);
    }
}

class Solution {
    public static void main(String[] args) {

        Demo obj1 = new Demo();
        Demo obj2 = new Demo();

        obj1.display();
        obj2.display();

        System.out.println("\n-----------");

        obj1.x = 100;
        obj2.z = 300;

        obj1.display();
        obj2.display();

        System.out.println();
    }
}