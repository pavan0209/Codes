/*
 *  Static variable in java. 
 *  - Static variable is also called as Class Variable.
 *  - Static variable is the first thing in java sequence while code execution.   
 *  - Static variable and method can be called using class name.
 *  - There is no need to create object for calling static things in java program.
 */

class StaticDemo {

    static int x = 10;
    static int y = 20;

    //Static Method
    static void display() {       

        System.out.println("\nx :: " + x);
        System.out.println("y :: " + y);
    }
} 

class Solution {
    public static void main(String[] args) {

        System.out.println("\nx :: " + StaticDemo.x);
        System.out.println("y :: " + StaticDemo.y);
        
        StaticDemo.display();

        System.out.println();
    }
}