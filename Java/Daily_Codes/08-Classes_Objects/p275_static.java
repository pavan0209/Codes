/*
 * 
 */

class Demo {

    int x = 10;
    static int y = 20;

    //Non-Static Method
    void fun1() {

        System.out.println("\nx :: " + x);
        System.out.println("y :: " + y);
    }

    //Static Method
    static void fun2() {       

        //System.out.println("\nx :: " + x);    //Error :
        System.out.println("y :: " + y);
    }
} 

class Client {
    public static void main(String[] args) {

        Demo obj = new Demo();

        obj.fun1();
        obj.fun2();

        System.out.println("\nx :: " + obj.x);
        System.out.println("y :: " + obj.y);
    
        System.out.println();
    }
}