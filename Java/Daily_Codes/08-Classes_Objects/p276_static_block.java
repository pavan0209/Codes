/*
 *  Static Block in JAVA.
 *  - Static variable are stored in Static Block.
 *  - Static block comes before main Method and called by JVM.
 *  - If static variable is present in code then and then only comes static block.
 */

class Solution {

    //Static Block
    static {

        System.out.println("\nIn Static Block");
    }

    public static void main(String[] args) {
        
        System.out.println("In Main Method");

        System.out.println();
    }
}