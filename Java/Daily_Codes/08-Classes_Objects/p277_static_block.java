/*
 *
 */

class Solution {

    static int x = 10;
    
    // Static Block
    static {

        System.out.println("\nIn Static Block");
        System.out.println("x :: " + x);
    }

    public static void main(String[] args) {

        System.out.println("In Main Method");

        System.out.println();
    }
}