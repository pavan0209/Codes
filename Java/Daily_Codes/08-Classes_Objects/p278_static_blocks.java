/*
 *  Static Blocks.
 *  - If there are more than one static block in code then execution is done squencewise.
 */

class Solution {

    // Static Block
    static {

        System.out.println("\nStatic Block-1");
    }

    public static void main(String[] args) {

        System.out.println("In Main Method");

        System.out.println();
    }
    
    //Static Block
    static {

        System.out.println("Static Block-2");
    }
}