/*
 *  Static Blocks.
 *  - If there are more than one static block in code then execution is done squencewise.
 */

 class Solution {

    int x = 10;
    static int y = 20;

    // Static Block
    static {

        System.out.println("\nStatic Block-1");
    }

    public static void main(String[] args) {

        System.out.println("In Main Method");

        Solution s1 = new Solution();
        
        System.out.println("x :: " + s1.x);
        
        System.out.println();
    }
    
    //Static Block
    static {

        System.out.println("Static Block-2");
        System.out.println("y :: " + y);
    }
}