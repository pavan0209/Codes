/*
 *  Static block is different for each class.
 */

class Demo {

    static {

        System.out.println("\nIn Static Block-1");
    }
}

class Solution {

    // Static Block
    static {

        System.out.println("\nIn Static Block-2");
    }

    public static void main(String[] args) {

        System.out.println("In Main Method");

        System.out.println();
    }

    // Static Block
    static {

        System.out.println("Static Block-3");
    }
}