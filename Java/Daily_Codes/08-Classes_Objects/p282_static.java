/*
 *  Where we can write static variable??
 *  - We can only write static variable in class.
 */

class Solution {

    static int x = 10;

    static{

        static int y = 20;
    }

    void fun() {

        static int z = 30;
    }

    static void gun() {

        static int m = 20;
    }
} 