/*
 *  Instance Variable in Java .
 *  - instance variable is also called as non-static variable.  
 */

class Solution {

    //instance / non-static variable
    int x = 10;

    public static void main(String[] args) {

        System.out.println("main");
    }
}