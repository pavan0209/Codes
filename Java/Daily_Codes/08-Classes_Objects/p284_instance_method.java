/*
 *  Instance Method in Java .
 *  - instance Method is also called as non-static Method. 
 *  - get stored in object 
 */

 class Solution {

    //instance / non-static variable
    int x = 10;

    static int y = 20;  //Static variable

    //instance method
    void fun() {

        System.out.println("\nx :: " + x);
        System.out.println("y :: " + y);
    }

    public static void main(String[] args) {

    }
}