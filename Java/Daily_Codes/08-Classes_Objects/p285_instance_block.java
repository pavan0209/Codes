/*
 *  Instance Block in Java.
 *  - stored in constructor.
 */

class Solution {

    int x = 10;

    Solution() {

        System.out.println("In Constructor");
    }

    //Instance block
    {
        System.out.println("In Instance Block-1");
    }

    public static void main(String[] args) {

        Solution obj = new Solution();
        
        System.out.println("in Main\n");
    }
}