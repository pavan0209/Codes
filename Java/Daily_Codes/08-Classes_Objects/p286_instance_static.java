/*
 
*/

class Solution {

    int x = 10;
    static int y = 20;

    Solution() {

        System.out.println("In Constructor");
    }

    static {

        System.out.println("In Static Block-1");
    }

    {
        System.out.println("In Instance Block-1");
    }

    public static void main(String[] args) {

        Solution obj = new Solution();
    }

    static {

        System.out.println("In Static Block-2");
    }

    {
        System.out.println("In Instance Block-2");
    }
}