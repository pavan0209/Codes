/*
 *  Hidden 'this' reference.
*/

class Solution {

    int x = 10;

    Solution() {

        System.out.println("In Constructor");
        System.out.println("x :: " + x);
        System.out.println("x :: " + this.x);
    }

    void fun() {
        
        System.out.println("x :: " + x);
    }

    public static void main(String[] args) {

        Solution obj = new Solution();
        obj.fun();
    }
}