/*
 *  this reference.
 */ 

 class Solution {
   
    int x = 10;

    Solution() {    // internal call => Solution(Solution this)

        System.out.println("\nIN Constructor\n");
        System.out.println("this :: " + this);
        System.out.println("x :: " + this.x);
    }

    void fun() {    // internal call => void fun(Solution this)

        System.out.println("this :: " + this);
        System.out.println("x :: " + this.x);
    }
   
    public static void main(String[] args) {

        Solution obj1 = new Solution();     //internal call => Solution(obj1)
        System.out.println("obj1 :: " + obj1);  

        obj1.fun();     //internal call => fun(obj1)
        System.out.println();
    }
}