/* 
*/

class Solution {

    Solution() {
        System.out.println("\nIn No-Args Constructor");
    }

    Solution(int x) {

        System.out.println("In Parameterized Constructor");
    }

    Solution(Solution xyz) {

        System.out.println("In Para-Solution");
    }

    public static void main(String[] args) {

        Solution obj1 = new Solution();
        Solution obj2 = new Solution(10);
        Solution obj3 = new Solution(obj2); 

        System.out.println();
    }
}