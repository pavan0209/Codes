/* 
*/

class Solution {

    int x = 10;

    Solution() {

        System.out.println("\nIn Constructor");
    }

    Solution() {    //error: constructor Solution() is already defined in class Solution

        System.out.println("In Constructor 2");
    }

    public static void main(String[] args) {

    }
}