/*
*/

class Solution {

    int x = 10;

    Solution() {     //Internal call => Solution(Solution this)

        System.out.println("\nIn No-Args Constructor");
    }

    Solution(int x) {   //internal call => Solution(Solution this, int x)

        System.out.println("In Parameterized Constructor");
    }

    public static void main(String[] args) {

        Solution obj1 = new Solution();     //internal call => Solution(obj1)
        Solution obj2 = new Solution(10); //internal call => Solution(obj2, this);
    }
}