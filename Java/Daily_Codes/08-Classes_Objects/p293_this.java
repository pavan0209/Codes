/*
 *  Calling Constructor from another constructor 
 */

class Demo {

    int x = 10;

    Demo() {

        System.out.println("In No-Args Constructor");
    }

    Demo(int x) {

        this();
        System.out.println("In Parameterized Constructor");
        //this();   //error: call to this must be first statement in constructor
    }
    
    public static void main(String[] args) {

        Demo obj = new Demo(30);
    }
}