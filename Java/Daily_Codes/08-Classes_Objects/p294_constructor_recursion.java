/*
 *  Calling Constructor from another constructor is allowed but there should not be a recusive call. 
 */

class Demo {

    int x = 10;

    Demo() {

        this(70);       //error: recursive constructor invocation
        System.out.println("In No-Args Constructor");
    }

    Demo(int x) {

        this();
        System.out.println("In Parameterized Constructor");
        // this(); //error: call to this must be first statement in constructor
    }

    public static void main(String[] args) {

        Demo obj = new Demo(30);
    }
}