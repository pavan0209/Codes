/*
 *  Calling Constructor from another constructor 
 */

 class Demo {

    int x = 10;

    Demo() {

        System.out.println("In No-Args Constructor");
    }

    Demo(int x) {

        this();
        this.x = x;
        System.out.println("In Parameterized Constructor");
        System.out.println(x);
        System.out.println(this.x);
    }
    
    public static void main(String[] args) {

        Demo obj = new Demo(50);
    }
}