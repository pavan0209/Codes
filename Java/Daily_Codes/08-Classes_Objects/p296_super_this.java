/*
 *  super and this doesn't work together.
 */

 class Demo {

    int x = 10;

    Demo() {
    
        this(20);
        System.out.println("In No-Args Constructor");
    }

    Demo(int x) {

       // this();
        super();     //error: call to super must be first statement in constructor
        System.out.println("In Parameterized Constructor");
        //this();   //error: call to this must be first statement in constructor
    }
    
    public static void main(String[] args) {

        Demo obj = new Demo(30);
    }
}