/*
 *  Assigning values to private variables using parameterized constructor. 
 */

class Player {

    private int jerNo = 0;
    private String name = null;

    Player(int jerNo, String name) {

        this.jerNo = jerNo;
        this.name = name;

        System.out.println("In Constructor");
    }

    void info() {

        System.out.println(jerNo + " :: " + name);
    }

    public static void main(String[] args) {

        Player p1 = new Player(18, "Virat");
        p1.info();

        Player p2 = new Player(7, "MSD");
        p2.info();

        Player p3 = new Player(45, "Rohit");
        p3.info();
    }
}