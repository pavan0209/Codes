/*
 *  Working of Static Block and Static Methods in Parent-Child relationship. 
*/

class Parent {

    static int x = 10;

    static {

        System.out.println("In Parent Static Block");
    }

    static void access() {

        System.out.println("x :: " + x);
    }
}

class Child extends Parent {

    static {

        System.out.println("In Child Static Block");
        System.out.println("x :: " + x);
        access();
    }
}

class Client {

    public static void main(String[] args) {

        Child b1 = new Child();
    }
}
