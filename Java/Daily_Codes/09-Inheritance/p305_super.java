/*
 *  super keyword in java.
 *      - When the variable names and method names are same in the parent class as well as in the child class
 *  then super keyword is used to access the things of Parent class.
 *  
 *  super() : calls the constructor of parent class
 *  this() : calls the constructor of own class. 
 */

class Parent {

    int x = 10;
    static int y = 20;

    Parent() {

        System.out.println("In Parent");
    }
}

class Child extends Parent {

    int x = 100;
    static int y = 200;

    Child() {

        System.out.println("In Child");
    }

    void access() {

        System.out.println("Parent x :: " + super.x);
        System.out.println("Parent y :: " + super.y);

        System.out.println("Child x :: " + this.x);
        System.out.println("Child y :: " + this.y);
    }
}

class Client {

    public static void main(String[] args) {
        
        Child obj = new Child();

        obj.access();
    }
}