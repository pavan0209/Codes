/*
    Same Method Signature doesn't allowed in same class
    In Method Overloading return type doesn't considered.
 */

class Solution {

    void fun(int x) {   //Method Signature : fun(int)

    }

    int fun(int x) {   ///Method Signature : fun(int)      //error: method fun(int) is already defined in class Solution

    }

    public static void main(String[] args) {

    }
}