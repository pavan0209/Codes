/*
 *    Method Overloading can be done in one class.
 */

class Solution {

    void fun(int x) {

        System.out.println("x :: " + x);
    }

    void fun(float y) {

        System.out.println("y :: " + y);
    }

    void fun(Solution s1) {

        System.out.println("In Solution Parameterized");
        System.out.println(s1);
    }

    public static void main(String[] args) {

        Solution s1 = new Solution();
        
        s1.fun(10);
        s1.fun(10.5f);

        Solution s2 = new Solution();
        s2.fun(s1);
    }
}