/*
 *  Method Overriding in JAVA.
 */

 class Parent {

    Parent() {

        System.out.println("\nIn Parent Constructor");
    }

   void fun() {

    System.out.println("In Fun");
   }
}

class Child extends Parent {

    Child() {

        System.out.println("In Child Constructor");
    }

    void gun() {

        System.out.println("In Gun\n");
    }
}


class Client {
    public static void main(String[] args) {

        Child obj1 = new Child();
        obj1.fun();
        obj1.gun();

        Parent obj2 = new Parent();
        obj2.fun();
        obj2.gun();     //error: cannot find symbol  => Parent class doesn't have method gun().

        Parent obj3 = new Child();  //Parent's reference and Child's Object is allowed

        Child obj4 = new Parent();  //error: incompatible types: Parent cannot be converted to Child
        
    }
}