/*
 *  Method Overriding in JAVA.
 *  Understanding Parent's reference and Child's Object
 */

 class Parent {

    Parent() {

        System.out.println("\nIn Parent Constructor");
    }

   void fun() {

        System.out.println("In Fun");
   }
}

class Child extends Parent {

    Child() {

        System.out.println("In Child Constructor");
    }

    void fun() {

        System.out.println("In Fun\n");
    }
}


class Client {
    public static void main(String[] args) {

        Parent obj = new Child();
        obj.fun();
        
    }
}