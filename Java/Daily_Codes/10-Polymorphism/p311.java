/*
 *  Method Overriding in JAVA.
 *  Understanding Parent's reference and Child's Object
 */

 class Parent {

    Parent() {

        System.out.println("\nIn Parent Constructor");
    }

   void fun(int x) {

        System.out.println("In Fun");
   }
}

class Child extends Parent {

    Child() {

        System.out.println("In Child Constructor");
    }

    void fun() {

        System.out.println("In Fun\n");
    }
}


class Client {
    public static void main(String[] args) {

        Parent obj = new Child();
        obj.fun();  //error: method fun in class Parent cannot be applied to given types;
                    //         obj.fun();
                    //         ^
                    // required: int
                    // found:    no arguments
                    // reason: actual and formal argument lists differ in length

                    //At compile time parent's method is bind and there is no matching function so error.
        
    }
}