/*
 *  Method Overriding in JAVA.
 *  Understanding Parent's reference and Child's Object
 */

 class Parent {

    Parent() {

        System.out.println("\nIn Parent Constructor");
    }

   void fun() {

        System.out.println("In Parent Fun");
   }
}

class Child extends Parent {

    Child() {

        System.out.println("In Child Constructor");
    }

    void fun(int x) {

        System.out.println("In Child Fun\n");
    }
}


class Client {
    public static void main(String[] args) {

        Parent obj = new Child();
        obj.fun();
        //obj.fun(10);    //error: method fun in class Parent cannot be applied to given types;
                        //         obj.fun(10);
                        //         ^
                        // required: no arguments
                        // found:    int
                        // reason: actual and formal argument lists differ in length
        
    }
}