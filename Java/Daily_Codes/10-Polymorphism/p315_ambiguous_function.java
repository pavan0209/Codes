/*
 *  Ambiguos Function Problem.
 */

class Demo {
    
    void fun(int x, float y) {      

        System.out.println("int-float parameters");
    }

    void fun(float x, int y) {

        System.out.println("float-int parameters");
    }
}

class Client {
    public static void main(String[] args) {
        
        Demo obj = new Demo();
        obj.fun(10, 10);        //error: reference to fun is ambiguous
                                    //Parameters to fun() is matching to both functions so its ambiguity.
    }
}