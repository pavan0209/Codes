/*
 *  Parent and Child Class as parameters of function.
 *  If things going to both Parent and Child, then priority is given to Child.
 *  Object Class can accept any type of data. 
 */

class Demo {

    void fun(Object obj) {

        System.out.println("Object");
    }

    void fun(String str) {

        System.out.println("String");
    }
}

class Client {
    public static void main(String[] args) {

        Demo obj = new Demo();
        obj.fun("Pavan");
        obj.fun(new StringBuffer("Pavan"));
        obj.fun(null);
    }
}