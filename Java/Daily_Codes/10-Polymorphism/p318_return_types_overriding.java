/*
 *  In Override a function Return type of function should be same except covarient return types
 */

class Parent {

    char fun() {

        System.out.println("In Parent fun");
        return 'A';
    }
}

class Child extends Parent{

    int fun() {     //error: fun() in Child cannot override fun() in Parent
                    //     int fun() {
                    //         ^
                    // return type int is not compatible with char
                    
        System.out.println("In Child Fun");
        return 10;
    }
}

class Client {
    public static void main(String[] args) {

        Parent p1 = new Child();
        p1.fun();
    }
}