/*
 *  In Overriding Covarient return types are allowed. Covarient means Retun types with parent child relationship.
 */

class Parent {

    Object fun() {
        
        Object obj = new Object();
        System.out.println("In Parent fun");
        return obj;

        //reuturn new Object();
    }
}

class Child extends Parent {

    String fun() {

        System.out.println("In Child fun");
        return "Pavan";
    }
}

class Client {
    public static void main(String[] args) {

        Parent p1 = new Child();
        p1.fun();
    }
}