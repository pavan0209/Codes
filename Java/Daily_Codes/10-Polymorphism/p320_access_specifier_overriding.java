/*
 *  Access Specifier in Overriding.
 */

class Parent {

    public void fun() {

        System.out.println("In Parent Fun");
    }
}

class Child extends Parent {

    void fun() {    //error: fun() in Child cannot override fun() in Parent
                    //     void fun() {
                    //         ^
                    // attempting to assign weaker access privileges; was public

        System.out.println("In Child Fun");
    }
}

class Client {
    public static void main(String[] args) {

        Parent obj = new Child();

        obj.fun();
    }
}