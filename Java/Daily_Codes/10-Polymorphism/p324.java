/*
 *  Static Modifier in Overriding.
 */

 class Parent {

    void fun() {

        System.out.println("In Parent Fun");
    }
}

class Child extends Parent {

    static void fun() {     //error: fun() in Child cannot override fun() in Parent
                            //     static void fun() {    
                            //                 ^
                            // overriding method is static 

        System.out.println("In Child Fun");
    }
}

class Client {
    public static void main(String[] args) {

        Parent obj = new Child();
        obj.fun();
    }
}