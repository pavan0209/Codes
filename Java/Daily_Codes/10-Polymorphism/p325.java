/*
 *  Static Modifier in Overriding.
 *  static doesn't take part in overriding if Parent and Child both have static method then this concept is called as method hiding.
 */

 class Parent {

    static void fun() {

        System.out.println("In Parent Fun");
    }
}

class Child extends Parent {

    static void fun() {     

        System.out.println("In Child Fun");
    }
}

class Client {
    public static void main(String[] args) {

        Parent obj1 = new Parent();
        obj1.fun();

        Child obj2 = new Child();
        obj2.fun();

        Parent obj = new Child();
        obj.fun();
    }
}