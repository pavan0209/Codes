/*
 *  mistakes of writing abstract method in JAVA.
 */

class Demo1 {

    void career() {

        System.out.println("Doctor");
    }

    void marry();       //error: missing method body, or declare abstract
}

class Demo2 {        //error: Demo2 is not abstract and does not override abstract method marry() in Demo2

    void career() {

        System.out.println("Doctor");
    }

    abstract void marry();
}