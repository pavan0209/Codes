/*
 *  Any method in class with no body should be declared with abstract keyword and the class in which abstract
 *  methods are declared should also be declared as abstract class.
 */

abstract class Parent {
    
    void career () {

        System.out.println("Doctor");
    }

    abstract void marry();
}

class Client {
    public static void main(String[] args) {

        Parent obj = new Parent();      //error: Parent is abstract; cannot be instantiated
                                    //Object of abstract class is not created because it is an incomplete class.
    }   
}