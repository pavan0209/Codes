/*
 *  The class which extends the abstract class should override the abstract method or declared as abstract.
 */

abstract class Parent {

    void career () {

        System.out.println("Doctor");
    }

    abstract void marry();
}

class Child extends Parent {    //error: Child is not abstract and does not override abstract method marry() in Parent


}