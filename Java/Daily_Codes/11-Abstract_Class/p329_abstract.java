/*
 *  Abstract class and method example.
 */

 abstract class Parent {

    void career () {

        System.out.println("Doctor");
    }

    abstract void marry();
}

class Child extends Parent {    //error: Child is not abstract and does not override abstract method marry() in Parent

    void marry() {

        System.out.println("Kriti Sanon");
    }
}

class Client {

    public static void main(String[] args) {
        
        Child obj = new Child();
        obj.career();
        obj.marry();

        Parent obj1 = new Child();
        obj1.career();
        obj1.marry();
    }
}