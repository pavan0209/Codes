/*
 *  Real-life Example of Abstract method
 */

abstract class EducationBoard {

    void aims() {

        System.out.println("All Students should educate");
    }

    abstract void syllabusInfo();
}

class SPPU extends EducationBoard {

    void syllabusInfo() {

        System.out.println("Engineering, Science, etc");
    }
}

class Client {
    public static void main(String[] args) {

        SPPU obj = new SPPU();
        obj.aims();
        obj.syllabusInfo();
    }
}