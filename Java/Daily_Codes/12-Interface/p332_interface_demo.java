/*
 *  Interface in JAVA.
 */

//creating interface
interface Demo {

    void fun();
    void gun();
    void run();
}

class Client {
    public static void main(String[] args) {

        Demo obj = new Demo();      //error: Demo is abstract; cannot be instantiated
                                    //Interface is also a class but it's an incomplete class hence 
                                    //object of interface can not be created.
    }
}