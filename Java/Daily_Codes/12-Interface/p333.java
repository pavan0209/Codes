/*
 *  Understanding errors in Interface.
 */

interface Demo1 {

    void m1() {     //error: interface abstract methods cannot have body

    }
}

interface Demo2 {

    void fun();
    void gun();
}

class DemoChild implements Demo2 {  //error: DemoChild is not abstract and does not override abstract method gun() in Demo2


}