/*
 *  Errors in Interface.
 */

interface Demo {

    void fun();     //By Default => public abstract void fun();
    void gun();     //By Default => public abstract void gun();
}

class DemoChild implements Demo {

    void fun() {        //attempting to assign weaker access privileges; was public

        System.out.println("In Fun");
    }

    void gun() {        //attempting to assign weaker access privileges; was public

        System.out.println("In Gun");
    }
}

class Solution {
    public static void main(String[] args) {

    }
}