/*
 *  Example of Interface.
 */

 interface Demo {

    void fun();     //By Default => public abstract void fun();
    void gun();     //By Default => public abstract void gun();
}

class DemoChild implements Demo {

    public void fun() {      

        System.out.println("In Fun");
    }

    public void gun() {        

        System.out.println("In Gun");
    }
}

class Client {
    public static void main(String[] args) {

        Demo obj = new DemoChild();
        obj.fun();
        obj.gun();
    }
}