/*
 *  Method body in interface.
 *  We can give a body to method in interface using 2 keywords default and static
 */

interface Demo {

    void fun();

    default void gun() {

    }

    static void run() {

    }
}
