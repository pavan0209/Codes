/*
 *  Default and Static in Interface.
 */

interface Demo {

    void gun();

    default void fun() {

        System.out.println("In Fun-Demo");
    }
}

class DemoChild implements Demo {

    public void gun() {

        System.out.println("In Gun-DemoChild");
    }
}

class Client {
    public static void main(String[] args) {

        Demo obj = new DemoChild();
        obj.gun();
        obj.fun();
    }
}