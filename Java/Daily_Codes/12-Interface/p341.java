/*
 *  Default.
 */

interface Demo1 {

    default void fun() {

        System.out.println("In Fun-Demo1");
    }
}

interface Demo2 {

    default void fun() {

        System.out.println("In Fun-Demo2");
    }
}

class Child implements Demo1, Demo2 {

    public void fun() {

        System.out.println("In Fun-Child");
    }
}

class Client {
    public static void main(String[] args) {

        Child obj1 = new Child();
        obj1.fun();

        Demo1 obj2 = new Child();
        obj2.fun();

        Demo2 obj3 = new Child();
        obj3.fun();
    }
}