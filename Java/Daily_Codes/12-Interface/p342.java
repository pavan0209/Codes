/*
 *  Static
 */

interface Demo {

    static void fun() {

        System.out.println("In fun");
    }
}

class Child implements Demo {


}

class Client {
    public static void main(String[] args) {

        Child obj = new Child();
        obj.fun();  //error: cannot find symbol
                    //         obj.fun();
                    //         ^
                    // symbol:   method fun()
                    // location: variable obj of type Child
    }
}