/*
 */

class Demo {

    static void fun() {

        System.out.println("In fun");
    }
}

class Child extends Demo {

}

class Client {
    public static void main(String[] args) {

        Child obj = new Child();
        obj.fun(); 
    }
}