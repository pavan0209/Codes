/*  Inner Class 
*/

class Outer{

    class Inner{

        void m1(){
            System.out.println("In m1-inner");

       }  
    } 
     void m2(){
            System.out.println("In m2-outer");

       }   
}
class Solution{
    public static void main(String[] args) {
        
        Outer obj = new Outer();
        obj.m2();

        //Object creating of inner class

        Outer.Inner obj2 = new Outer().new Inner();
        obj2.m1();
    }
}