/*  Inner Class 
  void main() in same class
*/

class Outer {

    class Inner {

        void m1() {

            System.out.println("In m1-inner");
        }
    }

    void m2() {

        System.out.println("In m2-outer");
    }

    public static void main(String[] args) {

        Inner obj2 = new Outer().new Inner();
        obj2.m1();
    }
}