class Parent{

    int x = 10;
    void m1(){

        System.out.println("In parent m1");
    }
}
class Child extends Parent{

    int a = 20;
    void m1(){

        System.out.println("In child m1");
    }
}
class Client{

    Client(Parent p){

        System.out.println("IN constructor parent");
        p.m1();
    }
    Client(Child c){

        System.out.println("IN constructor child");
        c.m1();
    }
    public static void main(String[] args) {

        Client obj1 = new Client(new Parent());
        Client obj2 = new Client(new Child());
        
    }
}