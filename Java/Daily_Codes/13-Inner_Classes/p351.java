/* */

class Demo{

    Demo(){

        System.out.println("In constructor- Demo");
    }
}
class DemoChild extends Demo{

    DemoChild(){

        System.out.println("In constructor- DemoChild");
    }
}
class Parent{

    Parent(){

        System.out.println("In constructor- Parent");
    }

    Demo m1(){

        System.out.println("In m1-Parent");
    }                                                           //error: missing return statement
}
class Child extends Parent{

    Child(){

        System.out.println("In constructor- Child");
    }

    DemoChild m1(){

        System.out.println("In m1-child");
    }
}
class Client{
    public static void main(String[] args) {
        
        Parent p = new Child();
        p.m1();
    }
}