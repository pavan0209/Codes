/*  */

class Outer{

    int x = 10;
    static int y = 20;

    void m1(){

        int a = 30;
        static int b = 30;              //error: illegal start of expression
    }
    public static void main(String[] args) {
        
        int p = 50;
        static int q = 60;              //error: class, interface, enum, or record expected
    }
}