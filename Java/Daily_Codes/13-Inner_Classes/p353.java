/* */

class Outer{

    int x = 10;
    static int y = 20;

    void m1(){

        int a = 30;
        final static int b = 30;                //error: modifier static not allowed here
    }
    public static void main(String[] args) {
        
        int p = 50;
         final static int q = 60;               //error: modifier static not allowed here
    }
}