/* Metgod Local Inner Class */

class Outer{

    void m1(){

        System.out.println("m1-outer");

        class Inner{

            void  InnerM1(){

                System.out.println("m1-Inner");
            }
        }
        Inner obj = new Inner();            //return new Inner()
        //obj.m1();                           // error: cannot find symbol
    }
    void m2(){

        System.out.println("m2-outer");
    }
}
class Client{
    public static void main(String[] args) {
        
        Outer obj = new Outer();
        obj.m1();                           //obj.m1().m1()
        obj.m2();
    }
}