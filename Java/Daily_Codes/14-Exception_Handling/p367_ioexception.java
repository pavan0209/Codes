/*  */

import java.io.*;
class Demo{

    void getData() {

        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

        System.out.println("Enter data");
        int data = Integer.parseInt(br.readLine());     //error: unreported exception IOException; must be caught or declared to be thrown

        System.out.println(data);
    }
    public static void main(String[] args) {
        
        Demo obj = new Demo();
        obj.getData();
    }
}