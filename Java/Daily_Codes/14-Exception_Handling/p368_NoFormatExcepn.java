/* NumberFormat Exception */

import java.io.*;
class Demo{

    void getData() throws IOException{

        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

        System.out.println("Enter data");
        int data = Integer.parseInt(br.readLine());     //Exception in thread "main" java.lang.NumberFormatException: For input string: "bhumi"

        System.out.println(data);
    }
    public static void main(String[] args)throws IOException {
        
        Demo obj = new Demo();
        obj.getData();
    }
}