
/* Arithmetic Exception */

class Demo{
    public static void main(String[] args)throws ArithmeticException {

        System.out.println("Start main");
        System.out.println(10/0);               //Exception in thread "main" java.lang.ArithmeticException: / by zero
        System.out.println("End main");
        
    }
}