/* Try- Catch */

import java.io.*;

class Demo{

    public static void main(String[] args)throws IOException {
        
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

        System.out.println("Enter String");
        String str = br.readLine();            
        System.out.println(str);

        int data  = 0;
        
        try{

            System.out.println("Enter data::  ");
            data = Integer.parseInt(br.readLine());
        }catch(NumberFormatException obj){

            data = Integer.parseInt(br.readLine());

        }
        System.out.println(data);
    }
}