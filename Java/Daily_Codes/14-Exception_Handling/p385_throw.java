/* throw keyword or Clause */

import java.util.*;
class Demo{
    public static void main(String[] args) {
        
        Scanner sc = new Scanner(System.in);

        System.out.println("Enter number::  ");
        int x = sc.nextInt();

        try{
            if(x == 0){

                throw new ArithmeticException("Divided by Zero");
            }
            System.out.println(10/x);
        }catch(ArithmeticException ae){

             System.out.println("Exception in thread " + Thread.currentThread().getName() + "  ");
             
             ae.printStackTrace();


             System.out.println("ToString ::  " + ae.toString());
             System.out.println("GetMessage ::  " + ae.getMessage());

             System.out.println(ae);
        }
    }
}